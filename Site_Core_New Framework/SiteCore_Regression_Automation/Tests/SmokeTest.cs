﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SiteCore_Regression_Automation.Pages;
using OpenQA.Selenium;
using System.Collections.Generic;
using System.Linq;
using OpenQA.Selenium.Interactions;
using System.Threading;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;
using SiteCore_Regression_Automation;
using AshleyAutomationLibrary;

namespace Ashley.QA.Automation.SiteCore
{
    [TestClass]
    [DeploymentItem(@"TestDriver", @"TestDriver")]
    [DeploymentItem(@"TestData", @"TestData")]

    public class SmokeTest
    {
        BillingPage BillingPage;
        CartPage CartPage;
        ConfirmOrderPage ConfirmOrderPage;
        CommonFunctions CommonFunctions;
        Footer Footer;
        Header Header;
        HomePage HomePage;
        LoginPage LoginPage;
        OrderDetailsPage OrderDetailsPage;
        PaymentPage PaymentPage;
        PayPalPage PayPalPage;
        ProductDetailsPage ProductDetailsPage;
        SearchResultsPage SearchResultsPage;
        ShippingPage ShippingPage;
        StoreLocatorPage StoreLocatorPage;
        ThankYouPage ThankYouPage;
        WelcomePage WelcomePage;

        #region Public Variables

        //IWebDriver driver = null;
        public string status = null;

        #endregion


        [TestMethod]
        [TestCategory("Smoke Test")]
        [Priority(1)]
        [WorkItem(111855)]
        [Description("ECS-Verify E2E sale as a new user using synchrony card")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", "|DataDirectory|\\TestData\\Data.csv", "Data#csv", DataAccessMethod.Sequential)]
        public void ECSVerifyE2ESaleAsANewUserUsingSynchronyCard()
        {
            #region TestData

            // string Category = TestContext.DataRow["Category"].ToString();
            string Category = TestContext.DataRow["Category"].ToString();
            string SubCategory = TestContext.DataRow["SubCategory"].ToString();
            string dropDown = TestContext.DataRow["AccountdropDown"].ToString();
            string firstName = TestContext.DataRow["FirstName"].ToString();
            string lastName = TestContext.DataRow["LastName"].ToString();
            Random randomGenerator = new Random();
            double randomInt = randomGenerator.NextDouble();
            string email = "venhatesh" + randomInt + "@abc.com";
            string address = TestContext.DataRow["SynchronyAddress"].ToString();
            string city = TestContext.DataRow["SynchronyCity"].ToString();
            string state = TestContext.DataRow["SynchronyState"].ToString();
            string zip = TestContext.DataRow["SynchronyZipCode"].ToString();
            string phone = TestContext.DataRow["Phone"].ToString();
            string ashleyCardNumber = TestContext.DataRow["AshleyCardNumber"].ToString();
            string financingOption = TestContext.DataRow["FinancingOption"].ToString();
            string userName = TestContext.DataRow["UserName"].ToString();
            string password = TestContext.DataRow["Password"].ToString();

            #endregion

            #region TestDataLog

            TestContext.WriteLog("URL - " + Config.driver.Url);
            TestContext.WriteLog("Browser - " + Config.browserName);

            #endregion

            #region Local Variable Decleration


            #endregion

            #region TestSteps

            try
            {

                Archive.WaitForPageLoad();
                if (Config.driver.Url.Contains("ashcomm9"))
                { 
                    HomePage.btn_noThankyouClose.Click();
                }
                else
                {
                    TestContext.WriteLog("No thank you popup");
                }

                //2. Mouse Hover My Account and Click Create Account
                status = CommonFunctions.myAccountOption(dropDown);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"));

                //3. Enter Details
                status = LoginPage.createAccount(firstName, lastName, email, password);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"));

                Assert.AreEqual("My Account", WelcomePage.txt_headers.Text, "The Login Title is Wrong");
                TestContext.WriteLog("My Account title validation Successful");

                //4, 5 and 6. Click on Address Book and Add Shipping
                status = WelcomePage.addShippingAddressBook(firstName, lastName, address, city, state, zip, phone, email);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"));

                //7. 8. Add Billing Address
                status = WelcomePage.addBillingAddressBook(firstName, lastName, address, city, state, zip, phone, email);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"));

                //Config.driver.Navigate().Refresh();
                //Archive.WaitForPageLoad();

                //((IJavaScriptExecutor)Config.driver).ExecuteScript("window.scrollTo(0, 0)");

                //9. Mouse Hover Category and Sub Category
                status = HomePage.subCategorySelection(Category, SubCategory);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"));

                //10, 11. Click on Check Price
                status = SearchResultsPage.checkPriceSearchResultsPage(zip);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"));

                //12. Click on Product
                status = SearchResultsPage.selectProductFromSearchResultsPage();
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"));

                Archive.WaitForPageLoad();
                //13. Enter Qty and Click on Add to Cart
                status = ProductDetailsPage.addToCart("", "1", false, true);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"));

                Archive.WaitForElement(By.ClassName("popupimg"));
                //Product Image
                Assert.AreEqual(true, ProductDetailsPage.img_checkOutOverlayPopup.Displayed);
                TestContext.WriteLog("-Product image is showing");

                //Product Name             
                List<IWebElement> popupList = ProductDetailsPage.title_checkOutOverlayPopup.GetDescendants();
                int index = popupList.IndexOf(popupList.First(x => x.TagName == "h5"));
                Assert.AreEqual(true, popupList[index].Displayed);
                TestContext.WriteLog("-Product name is showing");

                //Product Price
                Assert.AreEqual(true, ProductDetailsPage.title_checkOutOverlayPopup.Text.Contains("$"));
                TestContext.WriteLog("-Product price is showing");

                //Quantity
                Assert.AreEqual(true, ProductDetailsPage.title_checkOutOverlayPopup.Text.Contains("Quantity:"));
                TestContext.WriteLog("-Product quantity is showing");

                //Color
                Assert.AreEqual(true, ProductDetailsPage.title_checkOutOverlayPopup.Text.Contains("Color:"));
                TestContext.WriteLog("-Product color is showing");

                //14. Click on Continue Shopping
                ProductDetailsPage.btn_PDPContinueShopping.Click();
                Archive.WaitForPageLoad();
                TestContext.WriteLog("Clicked on Continue Shopping Button");

                //((IJavaScriptExecutor)Config.driver).ExecuteScript("window.scrollTo(0, 0)");

                //15. Search Product
                status = CommonFunctions.searchForProduct(SubCategory, true, false);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"));

                status = SearchResultsPage.selectProductFromSearchResultsPage(false, true);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"));

                //16. Click Add to Cart
                status = ProductDetailsPage.addToCart("", "1", false, true);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"));

                Archive.WaitForElement(By.ClassName("popupimg"));
                //Product Image
                Assert.AreEqual(true, ProductDetailsPage.img_checkOutOverlayPopup.Displayed);
                TestContext.WriteLog("-Product image is showing");

                //Product Name               
                List<IWebElement> popupList1 = ProductDetailsPage.title_checkOutOverlayPopup.GetDescendants();

                int index1 = popupList1.IndexOf(popupList1.First(x => x.TagName == "h5"));
                Assert.AreEqual(true, popupList1[index1].Displayed);
                TestContext.WriteLog("-Product name is showing");

                //Product Price
                Assert.AreEqual(true, ProductDetailsPage.title_checkOutOverlayPopup.Text.Contains("$"));
                TestContext.WriteLog("-Product price is showing");

                //Quantity
                Assert.AreEqual(true, ProductDetailsPage.title_checkOutOverlayPopup.Text.Contains("Quantity:"));
                TestContext.WriteLog("-Product quantity is showing");

                //Color
                Assert.AreEqual(true, ProductDetailsPage.title_checkOutOverlayPopup.Text.Contains("Color:"));
                TestContext.WriteLog("-Product color is showing");

                //17.Click Check Out
                ProductDetailsPage.btn_PDPCheckOut.Click();
                Archive.WaitForPageLoad();
                TestContext.WriteLog("Click on checkOut button successful");

                //18, 19, 20.Click Promo Code
                //IWebElement ele_txt_promoCodeField = CartPage.txt_promoCodeField.Init();
                //ele_txt_promoCodeField.EnterText("");
                //IWebElement ele_btn_promoCodeButton = CartPage.btn_promoCodeButton.Init();
                //ele_btn_promoCodeButton.ClickControl();
                //CommonFunction.waitForPageLoad();

                //21. Click Proceed to Check Out
                CartPage.btn_checkOutCartPage.Click();
                Archive.WaitForPageLoad();
                TestContext.WriteLog("Click on Proceed to checkOut button successful");

                //22. Enter Shipping
                status = ShippingPage.addShippingAddress(firstName, lastName, address, city, state, phone, zip, email);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"));

                //23. Enter Billing
                status = BillingPage.addBillingAddress(true);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"));

                //24, 25 Enter Valid Synchrony Card
                status = PaymentPage.enterAshleyAdvantageCardPayments(ashleyCardNumber, financingOption);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"));

                Archive.WaitForElement(By.XPath("//button[.='Submit order']"));
                new Actions(Config.driver).MoveToElement(ConfirmOrderPage.btn_submitOrder).Click().Perform();
                TestContext.WriteLog("successfully clicked on Submit Order button");

                Archive.WaitForPageLoad();
                Archive.WaitForElement(By.ClassName("brdialog-close"));

                new Actions(Config.driver).MoveToElement(ThankYouPage.btn_orderConfirmCloseOverlay).Click().Perform();
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
            #endregion
        }

        [TestMethod]
        [TestCategory("Smoke Test")]
        [Priority(1)]
        [WorkItem(111859)]
        [Description("ECS_Manage Wish List")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", "|DataDirectory|\\TestData\\Data.csv", "Data#csv", DataAccessMethod.Sequential)]
        //[DataSource("Microsoft.VisualStudio.TestTools.DataSource.TestCase", "http://aarcwivpap10797:8080/tfs/Ashley;Ashley.QA", "111859", DataAccessMethod.Sequential)]
        public void ECSManageWishList()
        {

            #region TestData

            string Category = TestContext.DataRow["Category"].ToString();
            string SubCategory = TestContext.DataRow["SubCategory"].ToString();
            string ListName1 = TestContext.DataRow["ListName1"].ToString();
            string ListName2 = TestContext.DataRow["ListName2"].ToString();
            string ListName3 = TestContext.DataRow["ListName3"].ToString();
            string EditName = TestContext.DataRow["EditName"].ToString();
            string ShareEmail = TestContext.DataRow["ShareEmail"].ToString();
            string ShareMessage = TestContext.DataRow["ShareMessage"].ToString();
            string WishListName1 = TestContext.DataRow["WishListName1"].ToString();
            string WishListName2 = TestContext.DataRow["WishListName2"].ToString();
            string WishListName3 = TestContext.DataRow["WishListName3"].ToString();
            string dropDown = TestContext.DataRow["LoginDropdown"].ToString();
            string userName = TestContext.DataRow["UserName"].ToString();
            string password = TestContext.DataRow["Password"].ToString();
            #endregion

            #region TestDataLog

            TestContext.WriteLog("URL - " + Config.driver.Url);
            TestContext.WriteLog("Browser - " + Config.browserName);

            #endregion

            #region Local Variable Decleration


            #endregion

            #region TestSteps

            try
            {

                Archive.WaitForPageLoad();
                if (Config.driver.Url.Contains("ashcomm9"))
                {
                    HomePage.btn_noThankyouClose.Click();
                }
                else
                {
                    TestContext.WriteLog("No thank you popup");
                }

                //3. Mouse Over My Account and Click Sign In
                status = CommonFunctions.myAccountOption(dropDown);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"));

                //4. Enter Email Password and Click Sign In
                status = LoginPage.signIn(userName, password);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"));


                string login_Title = WelcomePage.txt_headers.Text;

                if (login_Title != "My Account") ;
                Assert.AreEqual("My Account", login_Title, "The Login Title is Wrong");
                TestContext.WriteLog("My Account title validation Successful");

                //5. Click on the Wish Lists

                List<IWebElement> lst_lstHeaders = WelcomePage.lnk_listHeaders.GetDescendants();
                int option_Index = lst_lstHeaders.IndexOf(lst_lstHeaders.First(x => x.Text.Contains("Wish Lists")));
                lst_lstHeaders[option_Index].Click();
                Archive.WaitForPageLoad();

                String strData = WelcomePage.lnk_ActiveWishlist[0].Text;

                status = WelcomePage.deleteWishLIst();

                //8. Click on the Create New Wish List
                //9. Enter Wish List and Click Create
                //10. Close the Pop Up
                status = WelcomePage.createNewWishList(WishListName1);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"));

                //11. Click on the Create New Wish List
                //12. Enter Wish List and Click Create
                //13. Close the Pop Up
                status = WelcomePage.createNewWishList(WishListName2);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"));

                //14. Mouse Over Category and select Sub Category
                status = HomePage.subCategorySelection(Category, SubCategory);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"));

                //15. Mouse Over Product and Click on Quick View
                status = SearchResultsPage.selectProductFromSearchResultsPage();
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"));

                //16. Enter Quantity and CLick Add to Wish List
                status = ProductDetailsPage.addToList();
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"));

                ////Product Image assert
                //CommonFunction.waitForPageLoad();
                //if (ProductDetailsPage.img_wishListOverlay.Init().Displayed == false)
                //    WebArchive.WriteLog("Product image not showing");
                //Assert.AreEqual(true, ProductDetailsPage.img_wishListOverlay.Init().Displayed);
                //WebArchive.WriteLog("Product image is showing");

                //Product name assert
                //if (ProductDetailsPage.title_wishListOverlay.Displayed == false)
                //  Assert.AreEqual(true, ProductDetailsPage.title_wishListOverlay.Displayed);
                //TestContext.WriteLog("Product name is showing");

                //17. Select Create a Wish List From Drop Down
                SelectElement selectElement = new SelectElement(ProductDetailsPage.slct_PDPWishList);

                selectElement.SelectByText("CREATE A NEW LIST");
                Archive.WaitForPageLoad();
                Thread.Sleep(3000);

                //18. Enter Name and Click on Create Button
                ProductDetailsPage.txt_createWishList.SendKeys(WishListName3);
                ProductDetailsPage.btn_createButton.Click();
                Archive.WaitForPageLoad();
                Thread.Sleep(3000);
                TestContext.WriteLog("Wish List '" + WishListName3 + "' created");
                //19. Created WIsh List and and i Item added Message should display


                //20. Click on Continue Shopping BUtton              
                ProductDetailsPage.btn_wishListContinueShopping.Click();
                //WebArchive.WaitForPageLoad(500, true);
                Archive.WaitForPageLoad();
                Thread.Sleep(3000);

                ProductDetailsPage.img_AshleyLogo.Click();
                Archive.WaitForPageLoad();
                Thread.Sleep(8000);

                //21 Mouse Hover Category and Click Sub Category
                status = HomePage.subCategorySelection(Category, SubCategory);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"));


                //22. Click on Product Image
                status = SearchResultsPage.selectProductFromSearchResultsPage();
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"));


                //23. Click on Add to Wish List
                status = ProductDetailsPage.addToList();
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"));

                //Product Image

                Assert.AreEqual(true, ProductDetailsPage.img_checkOutOverlayPopup.Displayed);
                TestContext.WriteLog("Product image is showing");

                //Product Name
                List<IWebElement> popupList = ProductDetailsPage.title_checkOutOverlayPopup.GetDescendants();
                int index = popupList.IndexOf(popupList.First(x => x.TagName == "h5"));
                Assert.AreEqual(true, popupList[index].Displayed);
                TestContext.WriteLog("Product name is showing");

                //24. select the Created wish list from drop down
                selectElement = new SelectElement(ProductDetailsPage.slct_PDPWishList);
                selectElement.SelectByText(WishListName1);
                Archive.WaitForPageLoad();
                Thread.Sleep(3000);
                TestContext.WriteLog("Wish list '" + WishListName1 + "' selected from wishlist drop down");
                // 1 Item added message should get display

                //25 Click on the View WIsh List Button
                ProductDetailsPage.btn_wishListViewWishList.Click();
                Archive.WaitForPageLoad();
                Thread.Sleep(3000);
                TestContext.WriteLog("Displaying '" + WishListName1 + "' on wishlist view page");

                //Item Name 
                //Item Image 
                //Item Original Price 
                //Item Sales Price 
                //Quantiy 
                //Item ID  

                Archive.WaitForPageLoad();

                Assert.AreEqual(true, ProductDetailsPage.title_wishListItemName.Displayed);
                TestContext.WriteLog("Item name is showing");

                //26 to 32  - Share, Edit and Delete Wish List
                status = WelcomePage.listNames(ListName1, EditName);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"));

                status = WelcomePage.listNames(ListName3, null, ShareEmail, ShareMessage);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"));

                status = WelcomePage.listNames(ListName2);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"));

                WelcomePage.lnk_activeLists.Click();
                Archive.WaitForPageLoad();

                List<IWebElement> ActiveTab = WelcomePage.lnk_activeTab.GetDescendants();
                int count = ActiveTab.Count();
                ActiveTab[count - 1].Click();
                Archive.WaitForPageLoad();
                Thread.Sleep(3000);

                //33. Select the Wish List and Click on Add to Cart Button      
                WelcomePage.btn_AddToCart.Click();
                Archive.WaitForPageLoad();
                Thread.Sleep(5000);

                //CommonFunction.waitForPageLoad();
                TestContext.WriteLog(" Wish Lists added to the cart");

                WelcomePage.btn_ContinueShopping.Click();
                Archive.WaitForPageLoad();

                WelcomePage.lnk_wishLists.Click();
                Archive.WaitForPageLoad();
                Thread.Sleep(3000);

                List<IWebElement> lnk_activeListsChildren = WelcomePage.lnk_activeTab.GetDescendants();
                int Count = lnk_activeListsChildren.Count();

                //IReadOnlyList<IWebElement> allWishlistChildren = ele_lnk_activeLists.FindElements(By.XPath("./"));
                //String strWishLists = allWishlistChildren[0].Text;

                //string WishListCount = allWishlistChildren.Last().Text.Replace("(", "").Replace(")", "");

                string WishListCount = lnk_activeListsChildren[Count - 1].Text.Replace("(", "").Replace(")", "");
                if (!WishListCount.Contains("0"))
                {
                    Assert.Fail("The item does not disappear from the wishlist");
                }
            }

            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
            #endregion
        }

        [TestMethod]
        [TestCategory("Smoke Test")]
        [Priority(2)]
        [WorkItem(112105)]
        [Description("Sign In - Wish List")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", "|DataDirectory|\\TestData\\Data.csv", "Data#csv", DataAccessMethod.Sequential)]
        public void SignInWishList()
        {
            #region TestData

            string WishListName = TestContext.DataRow["WishListName1"].ToString();
            string dropDown = TestContext.DataRow["LoginDropdown"].ToString();
            string username = TestContext.DataRow["UserName"].ToString();
            string password = TestContext.DataRow["Password"].ToString();
            #endregion

            #region TestDataLog

            TestContext.WriteLog("URL - " + Config.driver.Url);
            TestContext.WriteLog("Browser - " + Config.browserName);

            #endregion

            #region Local Variable Decleration


            #endregion

            #region TestSteps
            try
            {

                TestContext.WriteLog("Browser launch Successful");

                Archive.WaitForPageLoad();
                if (Config.driver.Url.Contains("ashcomm9"))
                {
                    HomePage.btn_noThankyouClose.Click();
                }
                else
                {
                    TestContext.WriteLog("No thank you popup");
                }

                status = CommonFunctions.myAccountOption(dropDown);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"));

                //4. Enter Email Password and Click Sign In
                status = LoginPage.signIn(username, password);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"));

                string login_Title = WelcomePage.txt_headers.Text;
                Assert.AreEqual("My Account", login_Title, "The Login Title is Wrong");
                TestContext.WriteLog("My Account title validation Successful");


                List<IWebElement> lst_lstHeaders = WelcomePage.lnk_listHeaders.GetDescendants();
                int option_Index = lst_lstHeaders.IndexOf(lst_lstHeaders.First(x => x.Text.Contains("Wish Lists")));

                lst_lstHeaders[option_Index].Click();

                status = WelcomePage.deleteWishLIst();

                status = WelcomePage.createNewWishList(WishListName);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"));

                option_Index = WelcomePage.btn_RowWishList.IndexOf(WelcomePage.btn_RowWishList.First(x => x.Text.Contains(WishListName)));
                WelcomePage.btn_RowWishList[option_Index].Click();
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }

            #endregion
        }

        [TestMethod]
        [TestCategory("Smoke Test")]
        [Priority(2)]
        [WorkItem(112084)]
        [Description("Sign In Experience - My Account Page")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", "|DataDirectory|\\TestData\\Data.csv", "Data#csv", DataAccessMethod.Sequential)]
        public void SignInExperienceMyAccountPage()
        {
            #region TestData

            string username = TestContext.DataRow["UserName"].ToString();
            string password = TestContext.DataRow["Password"].ToString();
            string dropDown = TestContext.DataRow["LoginDropdown"].ToString();

            #endregion

            #region TestDataLog

            TestContext.WriteLog("URL - " + Config.driver.Url);
            TestContext.WriteLog("Browser - " + Config.browserName);


            #endregion

            #region Local Variable Decleration


            #endregion

            #region TestSteps

            try
            {
                // 2. Launch Browser
                TestContext.WriteLog("Browser launch Successful");
                //3. Close No Thank You Link
                Archive.WaitForPageLoad();
                if (Config.driver.Url.Contains("ashcomm9"))
                {
                    HomePage.btn_noThankyouClose.Click();
                }
                else
                {
                    TestContext.WriteLog("No thank you popup");
                }

                //4. My Account - Click Sign In
                status = CommonFunctions.myAccountOption(dropDown);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"));

                //5. Enter Email, Password and Click Sign In
                status = LoginPage.signIn(username, password);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"));

                //6. Validate My Account Title is Displayed
                string login_Title = WelcomePage.txt_headers.Text;
                Assert.AreEqual("My Account", login_Title, "The Login Title is Wrong");
                TestContext.WriteLog("My Account title validation Successful");


                List<IWebElement> lst_lstHeaders = WelcomePage.lnk_listHeaders.GetDescendants();

                //7. Account Info Link is Displayed
                int option_Index = lst_lstHeaders.IndexOf(lst_lstHeaders.First(x => x.Text.Contains("Account Info")));
                Assert.IsNotNull(option_Index, "Expected  is not present");
                TestContext.WriteLog("Account Info link assert verification is present");

                //8. Order History Link is Displayed
                option_Index = lst_lstHeaders.IndexOf(lst_lstHeaders.First(x => x.Text.Contains("Order History")));
                Assert.IsNotNull(option_Index, "Expected  is not present");
                TestContext.WriteLog("Order History link assert verification is present");

                //9. Address Book Link is Displayed
                option_Index = lst_lstHeaders.IndexOf(lst_lstHeaders.First(x => x.Text.Contains("Address Book")));
                Assert.IsNotNull(option_Index, "Expected  is not present");
                TestContext.WriteLog("Address Book link assert verification is present");

                //10. Wish Lists Link is Displayed
                option_Index = lst_lstHeaders.IndexOf(lst_lstHeaders.First(x => x.Text.Contains("Wish Lists")));
                Assert.IsNotNull(option_Index, "Expected  is not present");
                TestContext.WriteLog("Wish List link assert verification is present");

            }

            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
            #endregion
        }

        [TestMethod]
        [TestCategory("Smoke Test")]
        [Priority(2)]
        [WorkItem(112779)]
        [Description("Sign In - Order History")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", "|DataDirectory|\\TestData\\Data.csv", "Data#csv", DataAccessMethod.Sequential)]
        public void SignInOrderHistorySignInOrderHistory()
        {
            #region TestData


            string dropDown = TestContext.DataRow["LoginDropdown"].ToString();
            string username = TestContext.DataRow["UserName"].ToString();
            string password = TestContext.DataRow["Password"].ToString();

            #endregion

            #region TestDataLog

            TestContext.WriteLog("URL - " + Config.driver.Url);
            TestContext.WriteLog("Browser - " + Config.browserName);

            #endregion

            #region Local Variable Decleration


            #endregion

            #region TestSteps

            try
            {
                TestContext.WriteLog("Browser launch Successful");

                Archive.WaitForPageLoad();
                if (Config.driver.Url.Contains("ashcomm9"))
                {
                    HomePage.btn_noThankyouClose.Click();
                }
                else
                {
                    TestContext.WriteLog("No thank you popup");
                }

                status = CommonFunctions.myAccountOption(dropDown);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"));

                status = LoginPage.signIn(username, password);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"));

                string login_Title = WelcomePage.txt_headers.Text;
                Assert.AreEqual("My Account", login_Title, "The Login Title is Wrong");
                TestContext.WriteLog("My Account title validation Successful");

                List<IWebElement> lst_lstHeaders = WelcomePage.lnk_listHeaders.GetDescendants();
                int option_Index = lst_lstHeaders.IndexOf(lst_lstHeaders.First(x => x.Text.Contains("Order History")));

                lst_lstHeaders[option_Index].Click();
                TestContext.WriteLog("Click on 'Order History' under My Account section performed");
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
            #endregion
        }

        [TestMethod]
        [TestCategory("Smoke Test")]
        [Priority(1)]
        [WorkItem(130030)]
        [Description("Verify paypal  checkout using 3rd party mattress from payment page")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", "|DataDirectory|\\TestData\\Data.csv", "Data#csv", DataAccessMethod.Sequential)]
        //[DataSource("Microsoft.VisualStudio.TestTools.DataSource.TestCase", "http://aarcwivpap10797:8080/tfs/Ashley;Ashley.QA", "111859", DataAccessMethod.Sequential)]
        public void PaypalOrderPayment()
        {
            #region TestData

            string Category = TestContext.DataRow["Category"].ToString();
            string SubCategory = TestContext.DataRow["SubCategory"].ToString();
            string firstName = TestContext.DataRow["FirstName"].ToString();
            string lastName = TestContext.DataRow["LastName"].ToString();
            string email = TestContext.DataRow["Email"].ToString();
            string address = TestContext.DataRow["CreditAddress"].ToString();
            string city = TestContext.DataRow["CreditCity"].ToString();
            string state = TestContext.DataRow["CreditState"].ToString();
            string phone = TestContext.DataRow["Phone"].ToString();
            string zipCode = TestContext.DataRow["CreditZipCode"].ToString();

            string userName = TestContext.DataRow["UserName"].ToString();
            string password = TestContext.DataRow["Password"].ToString();
            string GuestCheckOut = "Continue As Guest";
            #endregion

            #region TestDataLog

            TestContext.WriteLog("URL - " + Config.driver.Url);
            TestContext.WriteLog("Browser - " + Config.browserName);

            #endregion

            #region Local Variable Decleration


            #endregion

            #region TestSteps

            try
            {

                Archive.WaitForPageLoad();
                if (Config.driver.Url.Contains("ashcomm9"))
                {
                    HomePage.btn_noThankyouClose.Click();
                }
                else
                {
                    TestContext.WriteLog("No thank you popup");
                }

                status = HomePage.subCategorySelection(Category, SubCategory);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"));

                status = SearchResultsPage.selectProductFromSearchResultsPage();
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"));

                status = ProductDetailsPage.addToCart(zipCode);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"));

                Archive.WaitForPageLoad();

                /*add to checkout page*/
                Archive.WaitForElement(By.PartialLinkText("CHECKOUT"));

                ProductDetailsPage.btn_PDPCheckOut.Click();
                //CommonFunction.waitForPageLoad();
                Archive.WaitForPageLoad();

                TestContext.WriteLog("Click on checkOut button successful");

                Archive.WaitForElement(By.XPath("/descendant::span[contains(.,'Proceed to Checkout')]"));

                CartPage.btn_checkOutCartPage.Click();
                Archive.WaitForPageLoad();

                TestContext.WriteLog("Click on Proceed to checkOut button successful");

                Archive.WaitForElement(By.ClassName("my-account-wrapper"));

                List<IWebElement> ele_checkout = LoginPage.div_overallWrapper.FindElements(By.TagName("a")).ToList();
                IWebElement ele_continueAsGuest = ele_checkout.First(x => x.Text.Contains(GuestCheckOut));
                ele_continueAsGuest.Click();
                Archive.WaitForPageLoad();
                TestContext.WriteLog("Click on Continue As Guest button successful");

                status = ShippingPage.addShippingAddress(firstName, lastName, address, city, state, phone, zipCode, email);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"));

                Archive.WaitForPageLoad();

                status = BillingPage.addBillingAddress(true);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"));

                status = PaymentPage.payPalPayment("QA_Buyer26@ashleyfurniture.com", "QABuyer26!");
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"));

                Thread.Sleep(30000);
                Assert.IsNotNull(ConfirmOrderPage.img_paypalLogo);
                Assert.IsNotNull(ConfirmOrderPage.elm_shippingAddress);
                Assert.IsNotNull(ConfirmOrderPage.elm_billingAddress);
                TestContext.WriteLog("PayPal logo, Shipping Address and Billing Address verification successful");

                Thread.Sleep(10000);


                ConfirmOrderPage.btn_PlaceOrder.MouseOver();
                TestContext.WriteLog("Click on PlaceOrder button is successful");

                Thread.Sleep(50000);


                ThankYouPage.btn_orderConfirmCloseOverlay.MouseOver();
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message + ex.StackTrace);
            }
            #endregion
        }

        [TestMethod]
        [TestCategory("Smoke Test")]
        [Priority(1)]
        [WorkItem(112781)]
        [Description("Add 1 HD and 1 DS item and checkout as guest user - pay via credit card")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", "|DataDirectory|\\TestData\\Data.csv", "Data#csv", DataAccessMethod.Sequential)]
        //[DataSource("Microsoft.VisualStudio.TestTools.DataSource.TestCase", "http://aarcwivpap10797:8080/tfs/Ashley;Ashley.QA", "112781", DataAccessMethod.Sequential)]
        public void payViaCreditCardHD_and_DS_Items()
        {
            #region TestData

            string Category = TestContext.DataRow["Category"].ToString();
            string SubCategory = TestContext.DataRow["SubCategory"].ToString();
            string firstName = TestContext.DataRow["FirstName"].ToString();
            string lastName = TestContext.DataRow["LastName"].ToString();
            string email = TestContext.DataRow["Email"].ToString();
            string address = TestContext.DataRow["CreditAddress"].ToString();
            string city = TestContext.DataRow["CreditCity"].ToString();
            string state = TestContext.DataRow["CreditState"].ToString();
            string phone = TestContext.DataRow["Phone"].ToString();
            string zipCode = TestContext.DataRow["CreditZipCode"].ToString();
            string productCode = "a60000050";
            string quantity = "2";
            string zip = TestContext.DataRow["CreditZipCode"].ToString();
            string userName = TestContext.DataRow["UserName"].ToString();
            string password = TestContext.DataRow["Password"].ToString();
            string creditCardNumber = TestContext.DataRow["CreditCardNumber"].ToString();
            string securityCode = TestContext.DataRow["SecurityCode"].ToString();
            string expMonth = "March (03)";
            string expYear = "2018";
            string GuestCheckOut = "Continue As Guest";
            #endregion

            #region TestDataLog

            TestContext.WriteLog("URL - " + Config.driver.Url);
            TestContext.WriteLog("Browser - " + Config.browserName);

            #endregion

            #region Local Variable Declaration


            #endregion

            #region TestSteps

            try
            {
                //2. Launch Browser 

                TestContext.WriteLog("Browser launch Successful");

                Archive.WaitForPageLoad();
                if (Config.driver.Url.Contains("ashcomm9"))
                {
                    HomePage.btn_noThankyouClose.Click();
                }
                else
                {
                    TestContext.WriteLog("No thank you popup");
                }

                //3, 4. Mouse Hover Category and Sub Category
                status = HomePage.subCategorySelection(Category, SubCategory);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"));
                Archive.WaitForPageLoad();

                //5. Click on Product

                status = SearchResultsPage.selectProductFromSearchResultsPage();
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"));
                Archive.WaitForPageLoad();

                ////6. Verify Product details assert
                IWebElement ele_div_heading = ProductDetailsPage.div_mainContent.FindElement(By.TagName("h1"));
                Assert.IsNotNull(ele_div_heading);

                Assert.IsNotNull(ProductDetailsPage.div_imageContainer);

                Assert.IsNotNull(ProductDetailsPage.div_SocialMediaContainer);

                //7. Enter Qty and Click on Add to cart
                status = ProductDetailsPage.addToCart(zip, quantity);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"));

                //8. Click on Check Out
                Archive.WaitForElement(By.XPath("//a[.='CHECKOUT']"));
                ProductDetailsPage.btn_PDPCheckOut.Click();
                Archive.WaitForPageLoad();
                //Thread.Sleep(12 * 1000);
                TestContext.WriteLog("Click performed on CheckOut button");

                /*enter code to add a product a60000050  in cart and then select the product and checkout */
                //9. Search for Product
                status = CommonFunctions.searchForProduct(productCode, true, false);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"));

                status = SearchResultsPage.selectProductFromSearchResultsPage(false, true);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"));

                status = ProductDetailsPage.addToCart(zip, "1", false, true);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"));

                //10. Click on Checkout
                Archive.WaitForElement(By.XPath("//a[.='CHECKOUT']"));
                ProductDetailsPage.btn_PDPCheckOut.Click();
                //CommonFunction.waitForPageLoad();
                //Thread.Sleep(12 * 1000);
                Archive.WaitForPageLoad();
                TestContext.WriteLog("Click on checkOut button successful");

                //11. Verify Shipping Charge
                //12. Verify Product details
                //Product Image
                //Product Name
                //Sku Number
                //Fulfilled By
                //Item Price
                //Quantity
                //Apply Promo Code

                //CommonFunction.waitForPageLoad();
                //// Shipping Charge
                Archive.WaitForElement(By.ClassName("msax-EstHomeDelivery"));
                Assert.AreEqual(true, CartPage.lbl_shippingCharge.Displayed);
                TestContext.WriteLog("-Shipping charge is showing");

                //Apply Promo Code
                Assert.AreEqual(true, CartPage.btn_promoCodeButton.Displayed);
                TestContext.WriteLog("-Apply promoCode button is showing");

                Assert.AreEqual(true, CartPage.btn_checkOutCartPage.Displayed);
                TestContext.WriteLog("-Checkout button is showing");

                Assert.AreEqual(true, CartPage.lnk_continueShoppingAtCartPage.Displayed);
                TestContext.WriteLog("-Continue Shopping button is showing");

                //Sku Number
                Assert.AreEqual(true, CartPage.lbl_skuNumberAtCartPage[0].Displayed);
                TestContext.WriteLog("-SKU number is showing");

                //Fulfilled By
                Assert.AreEqual(true, CartPage.lbl_fulfilledByAtCartPage[0].Displayed);
                TestContext.WriteLog("-Fulfilled By is showing");

                //Product Image
                Assert.AreEqual(true, CartPage.lbl_productImageAtCartPage[0].Displayed);
                TestContext.WriteLog("-Product image is showing");

                //Product Name
                Assert.AreEqual(true, CartPage.lbl_productNameAtCartPage[0].Displayed);
                TestContext.WriteLog("-Product name is showing");

                //Quantity
                Assert.AreEqual(true, CartPage.lbl_productQuantityAtCartPage[0].Displayed);
                TestContext.WriteLog("-Product quantity is showing");

                //13. Click Proceed to Check Out
                CartPage.btn_checkOutCartPage.Click();
                // CommonFunction.waitForPageLoad();
                Archive.WaitForPageLoad();
                //Thread.Sleep(12 * 1000);
                TestContext.WriteLog("Click on Proceed to checkOut button successful");

                //14, 15. Click Continue as Guest
                List<IWebElement> ele_checkout = LoginPage.div_overallWrapper.FindElements(By.TagName("a")).ToList();
                IWebElement ele_continueAsGuest = ele_checkout.First(x => x.Text.Contains(GuestCheckOut));
                ele_continueAsGuest.Click();
                Archive.WaitForPageLoad();
                TestContext.WriteLog("Click on Continue As Guest button successful");

                IWebElement ele_tr_ShippingCharge = ShippingPage.ele_div_overallContainer.FindElement(By.ClassName("msax-EstHomeDelivery"));
                Assert.IsNotNull(ele_tr_ShippingCharge.Text);

                //16. Enter Shipping
                status = ShippingPage.addShippingAddress(firstName, lastName, address, city, state, phone, zipCode, email);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"));

                //17,18 Enter Billing
                status = BillingPage.addBillingAddress(true);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"));

                //19. Payment details
                IWebElement ele_paymentHeading = PaymentPage.div_mainwrapper.FindElement(By.TagName("h2"));
                Assert.IsNotNull(ele_paymentHeading.Text);

                //20. Enter credit Card

                status = PaymentPage.enterCreditPayments(firstName, lastName, creditCardNumber, securityCode, expMonth, expYear);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"));

                //21. Confirm order page

                //Payment Type

                Archive.WaitForPageLoad();

                // Shipping address
                Assert.AreEqual(true, ConfirmOrderPage.elm_shippingAddress.Displayed);
                TestContext.WriteLog("-Shipping Address is showing");

                //Billing Address
                Assert.AreEqual(true, ConfirmOrderPage.elm_billingAddress.Displayed);
                TestContext.WriteLog("-Billing Address is showing");

                //Shipping Method
                Assert.AreEqual(true, ConfirmOrderPage.elm_shippingMethod.Displayed);
                TestContext.WriteLog("-Shipping method is showing");

                //Order SUmmary
                Assert.AreEqual(true, ConfirmOrderPage.elm_orderSummary.Displayed);
                TestContext.WriteLog("-Order Summary section is showing");

                //Quantity
                Assert.AreEqual(true, ConfirmOrderPage.txt_quantity[0].Displayed);
                TestContext.WriteLog("-Quantity is showing");

                //Product name
                Assert.AreEqual(true, ConfirmOrderPage.lnk_productName[0].Displayed);
                TestContext.WriteLog("-Product name is showing");

                //Item Code
                Assert.AreEqual(true, ConfirmOrderPage.elm_productId[0].Displayed);
                TestContext.WriteLog("-Item code is showing");

                //22. Click SUbmit
                Archive.WaitForElement(By.XPath("//button[.='Submit order']"));
                new Actions(Config.driver).MoveToElement(ConfirmOrderPage.btn_submitOrder).Click().Perform();
                TestContext.WriteLog("successfully clicked on Submit Order button");

                Archive.WaitForPageLoad();
                //Thread.Sleep(50000);

                Archive.WaitForElement(By.ClassName("brdialog-close"));
                new Actions(Config.driver).MoveToElement(ThankYouPage.btn_orderConfirmCloseOverlay).Click().Perform();

                /*get the confirmation number*/

                // Header.selectMyAccountFunction("Order Status");
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message + ex.StackTrace);
            }
            #endregion
        }

        [TestMethod]
        [TestCategory("Smoke Test")]
        [Priority(1)]
        [WorkItem(143705)]
        [Description("Verify website as a profiled user")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", "|DataDirectory|\\TestData\\Data.csv", "Data#csv", DataAccessMethod.Sequential)]
        public void VerifyWebsiteAsAProfiledUser()
        {
            #region TestData

            string Category = TestContext.DataRow["Category"].ToString();
            string SubCategory = TestContext.DataRow["SubCategory"].ToString();
            string dropDown = TestContext.DataRow["AccountdropDown"].ToString();
            string firstName = TestContext.DataRow["FirstName"].ToString();
            string lastName = TestContext.DataRow["LastName"].ToString();
            string password = TestContext.DataRow["Password"].ToString();
            Random randomGenerator = new Random();
            double randomInt = randomGenerator.NextDouble();
            string email = "venhatesh" + randomInt + "@abc.com";
            string address = TestContext.DataRow["SynchronyAddress"].ToString();
            string city = TestContext.DataRow["SynchronyCity"].ToString();
            string state = TestContext.DataRow["SynchronyState"].ToString();
            string zip = TestContext.DataRow["SynchronyZipCode"].ToString();
            string phone = TestContext.DataRow["Phone"].ToString();
            string ashleyCardNumber = TestContext.DataRow["AshleyCardNumber"].ToString();
            string financingOption = TestContext.DataRow["FinancingOption"].ToString();
            string WishListName = TestContext.DataRow["WishListName1"].ToString();
            string creditCardNumber = TestContext.DataRow["CreditCardNumber"].ToString();
            string securityCode = TestContext.DataRow["SecurityCode"].ToString();
            string expMonth = "March (03)";
            string expYear = "2018";
            string browser = Config.browserName;
            #endregion

            #region TestDataLog

            TestContext.WriteLog("URL - " + Config.driver.Url);
            TestContext.WriteLog("Browser - " + Config.browserName);

            #endregion

            #region Local Variable Decleration
            string status = null;

            #endregion

            #region TestSteps

            try
            {
                //1. No Thank you popup close
                Archive.WaitForPageLoad();
                if (Config.driver.Url.Contains("ashcomm9"))
                {
                    HomePage.btn_noThankyouClose.Click();
                }
                else
                {
                    TestContext.WriteLog("No thank you popup");
                }

                //2. Mouse Hover My Account and Click Create Account
                status = CommonFunctions.myAccountOption(dropDown);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"));

                //3. Enter Details
                status = LoginPage.createAccount(firstName, lastName, email, password);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"));

                string login_Title = WelcomePage.txt_headers.Text;
                Assert.AreEqual("My Account", login_Title, "The Login Title is Wrong");
                TestContext.WriteLog("My Account title validation Successful");

                //4, 5 and 6. Click on Address Book and Add Shipping
                status = WelcomePage.addShippingAddressBook(firstName, lastName, address, city, state, zip, phone, email);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"));

                //7. 8. Add Billing Address
                status = WelcomePage.addBillingAddressBook(firstName, lastName, address, city, state, zip, phone, email);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"));

                Archive.WaitForElement(By.XPath("//ul[@class='tabs vertical large-3 column hide-for-mobile']"));
                List<IWebElement> lst_lstHeaders = WelcomePage.lnk_listHeaders.GetDescendants();
                int option_Index = lst_lstHeaders.IndexOf(lst_lstHeaders.First(x => x.Text.Contains("Wish Lists")));
                IJavaScriptExecutor jse = (IJavaScriptExecutor)Config.driver;
                jse.ExecuteScript("window.scrollBy(0,-1000)", "");

                lst_lstHeaders[option_Index].MouseOver();
                lst_lstHeaders[option_Index].Click();

                status = WelcomePage.deleteWishLIst();
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"));

                status = WelcomePage.createNewWishList(WishListName);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"));

                //IWebDriver driver = GlobalProperties.driver;
                //driver.Navigate().Refresh();
                //WebArchive.WaitForPageLoad();

                //((IJavaScriptExecutor)driver).ExecuteScript("window.scrollTo(0, 0)");
                Archive.WaitForElement(By.ClassName("afhs-logo-img"));
                ProductDetailsPage.img_AshleyLogo.Click();

                Archive.WaitForPageLoad();

                //9. Mouse Hover Category and Sub Category
                status = HomePage.subCategorySelection(Category, SubCategory);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"));

                //12. Click on Product
                status = SearchResultsPage.selectProductFromSearchResultsPage(true);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"));

                Archive.WaitForPageLoad();
                //13. Enter Qty and Click on Add to Cart
                status = ProductDetailsPage.addToCart(zip, "1", true);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"));

                Archive.WaitForPageLoad();
                Thread.Sleep(2000);

                Archive.WaitForElement(By.XPath("//button[.='Continue Shopping']"));

                ProductDetailsPage.btn_continueShopping.Click();
                TestContext.WriteLog("Click performed on Continue Shopping button");

                SearchResultsPage.img_AshleyLogo.Click();

                Archive.WaitForPageLoad();

                status = HomePage.subCategorySelection(Category, SubCategory);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"));

                //12. Click on Product
                status = SearchResultsPage.selectProductFromSearchResultsPage(true);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"));

                Archive.WaitForPageLoad();
                //13. Enter Qty and Click on Add to Cart
                status = ProductDetailsPage.addToCart("", "1", true);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"));

                Archive.WaitForPageLoad();

                Archive.WaitForElement(By.XPath("//button[.='Continue Shopping']"));

                ProductDetailsPage.btn_continueShopping.Click();
                TestContext.WriteLog("Click performed on Continue Shopping button");

                status = SearchResultsPage.selectProductFromSearchResultsPage();
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"));

                //13. Enter Qty and Click on Add to Cart
                status = ProductDetailsPage.addToCart("", "1", false, true);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"));

                Archive.WaitForElement(By.LinkText("CHECKOUT"));

                //12. Click on Check Out Button
                ProductDetailsPage.btn_PDPCheckOut.Click();

                TestContext.WriteLog("Click on checkOut button successful");

                //21. Click Proceed to Check Out

                Archive.WaitForElement(By.XPath("/descendant::span[contains(.,'Proceed to Checkout')]"));
                CartPage.btn_checkOutCartPage.Click();

                TestContext.WriteLog("Click on Proceed to checkOut button successful");

                Archive.WaitForPageLoad();
                //22. Enter Shipping
                status = ShippingPage.addShippingAddress(firstName, lastName, address, city, state, phone, zip, email);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"));

                //23. Enter Billing
                status = BillingPage.addBillingAddress(true);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"));

                status = PaymentPage.enterCreditPayments(firstName, lastName, creditCardNumber, securityCode, expMonth, expYear);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"));

                Archive.WaitForPageLoad();
                Archive.WaitForElement(By.XPath("//span[contains(.,'(Edit)')]"));
                ConfirmOrderPage.lnk_Address[2].Click();
                Thread.Sleep(5000);

                status = PaymentPage.enterAshleyAdvantageCardPayments(ashleyCardNumber, financingOption);
                TestContext.WriteLog(status);
                Assert.IsFalse(status.Contains("Failure"));


                ConfirmOrderPage.lnk_Address[2].Click();
                Thread.Sleep(5000);

                Archive.WaitForElement(By.Id("pay-pal"));
                IWebElement list_paymentType = PaymentPage.div_mainwrapper.FindElement(By.Id("pay-pal"));
                IWebElement ele_paypapRadiobtn = list_paymentType.FindElement(By.ClassName("radio-option"));
                ele_paypapRadiobtn.Click();
                Archive.WaitForElement(By.ClassName("paypal-button-content"));
                PaymentPage.btn_paypalCheckout.Click();
                Thread.Sleep(3000);

                //status = PaymentPage.payPalPayment("QA_Buyer26@ashleyfurniture.com", "QABuyer26!");
                //TestContext.WriteLog(status);
                //Assert.IsFalse(status.Contains("Failure"));

            }

            catch (Exception ex)
            {
                Assert.Fail(ex.Message + ex.StackTrace);
            }
            #endregion

        }



        [TestInitialize]
        public void TestStartup()
        {

            Archive.SetConfig(TestContext);
            Archive.LaunchDriver();
            IWebDriver a = Config.driver;
            BillingPage = new BillingPage();
            CartPage = new CartPage();
            ConfirmOrderPage = new ConfirmOrderPage();
            CommonFunctions = new CommonFunctions();
            Footer = new Footer();
            Header = new Header();
            HomePage = new HomePage();
            LoginPage = new LoginPage();
            OrderDetailsPage = new OrderDetailsPage();
            PaymentPage = new PaymentPage();
            PayPalPage = new PayPalPage();
            ProductDetailsPage = new ProductDetailsPage();
            SearchResultsPage = new SearchResultsPage();
            ShippingPage = new ShippingPage();
            StoreLocatorPage = new StoreLocatorPage();
            ThankYouPage = new ThankYouPage();
            WelcomePage =new WelcomePage();

            //Config.driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(10));
        }

        [TestCleanup]
        public void TestCleanup()
        {
            Config.driver.Quit();
        }


        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;
    }
}

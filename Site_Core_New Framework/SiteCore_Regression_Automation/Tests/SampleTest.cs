﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiteCore_Regression_Automation.Pages;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using AshleyAutomationLibrary;
using OpenQA.Selenium.Interactions;

namespace SiteCore_Regression_Automation.Tests
{
    [TestClass]
    [DeploymentItem(@"TestDriver", @"TestDriver")]

    public class SampleTest
    {
        CommonFunctions CommonFunctions;
        HomePage HomePage;
        SearchResultsPage SearchResultsPage;

        [TestMethod]
        [TestCategory("Smoke Test")]
        [Priority(2)]
        [WorkItem(112084)]
        [Description("Sign In Experience - My Account Page")]
        public void sampleScript()
        {
            //cf.LaunchDriver();

            HomePage homePage = new HomePage();
            Thread.Sleep(8000);
            homePage.ele_myAccount.Click();

        }

        public static string status = string.Empty;
        [TestMethod]
        [TestCategory("Regression Test")]
        [Priority(2)]
        [WorkItem(104193)]
        [Description("verify Search,SortBy and NarrowBy functionality")]
        public void verifySearchSortByNarrowByFunctionality()
        {
           
            //CommonFunctions CommonFunctions = new CommonFunctions();
            //SearchResultsPage SearchResultsPage = new SearchResultsPage();
            //HomePage HomePage = new HomePage();
            #region Testdata

            String Category = "Furniture";
            String SubCategory = "Loveseats";
            String SortByDropDownDefaultValue = "Featured";
            String zipCode = "60610";
            String PriceLowToHigh = "Prices: Low to High";
            String PriceHighToLow = "Prices: High to Low";
            String AlphabeticalAToZ = "Alphabetical: A-Z";
            String OnSale = "On Sale";
            String FreeStandardShipping = "Free Standard Shipping";
            String PriceFilterValue = "$500-$750";
            String ProductName = "Stool";


            #endregion

            Config.driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(10));
            //2. Enter search keyword in search textbox

            //status = CommonFunctions.searchForProduct(ProductName, true, false);
            // if (status.Contains("Error"))
            //{
            //   Console.WriteLine("'searchForProduct()' not performed for product code '" + ProductName + "' :- " + status);
            //   Assert.Fail(status);
            // }

            Archive.WriteLog(TestContext, "Search performed for product code '" + ProductName + "'");
            TestContext.WriteLog("Search performed for product code '" + ProductName + "'");
            // Search results page validation for product name

            //product count
            //string[] strSplitFromInformation = SearchResultsPage.ProductCountFromSearchInformation.Text.Split('f');
            // String strActualCountFromSearchInfo = strSplitFromInformation[1].Trim();

            //product count from header
            //String[] strCount = SearchResultsPage.productCountInHeader.Text.Split(')');
            //String strCountFinal = strCount[0].Replace("(", "");

            //Assert.AreEqual(strActualCountFromSearchInfo, strCountFinal, "FAILED: Product count validation based on search keyword");
            //Console.WriteLine("PASSED:Product count validation based on search keyword");

            //Assert.IsTrue(CommonFunctions.verifyElement(SearchResultsPage.searchResultInformation), "FAILED: Search result information validation");
            Console.WriteLine("PASSED:Search result information validation");


            //Archive.WaitForPageLoad();

            //Archive.WaitForElement(By.Id("imgChatTop2"));

            //SearchResultsPage.ele_LaunchOverlayClose.Click();

            Archive.WaitForElement(By.Id("USI_toolbar_all"));

             List<IWebElement> lstBottomOverLayElements=SearchResultsPage.ele_BottomOverlayClose.GetDescendants();

            int index = lstBottomOverLayElements.IndexOf(lstBottomOverLayElements.First(x => x.GetAttribute("alt")=="close"));
            lstBottomOverLayElements[index].Click();

            //Thread.Sleep(3000);

            //SearchResultsPage.btn_MyAccount.Click();

            //Thread.Sleep(3000);

            //SearchResultsPage.btn_SignIn.Click();

            //Config.driver.FindElement(By.XPath("//div[@id='USI_toolbar_all']/descendant::img[@alt='close']")).Click();

            //div[@id='USI_toolbar_all']/descendant::img[@alt='close']

            //3.Mouseover subcategory under Category

            status = HomePage.subCategorySelection(Category, SubCategory);
            if (status.Contains("Error"))
            {
                Console.WriteLine("'subCategorySelection()' not performed :- " + status);
                Assert.Fail(status);
            }

            //4.click on "Enter your zip code" under "To view all local pricing"

             Assert.IsTrue(SearchResultsPage.clickEnterZipCodeUnderToViewAllLocalPricing());

            //5.Enter the delivery ZIP code and click on search button

            //Thread.Sleep(6000);

           Assert.IsTrue(SearchResultsPage.enterZIPCodeInSearchResultsPageZIPCodePopUp(zipCode, "Add ZIP code in ZIP code pop up"));

            //Thread.Sleep(5000);

            List<IWebElement> lst_Header = Config.driver.FindElement(By.ClassName("product-facets__header")).GetDescendants();
            //List <IWebElement> lst_Header = Config.driver.FindElements(By.ClassName("product-facets__header")).ToList();

            int no = lst_Header.IndexOf(lst_Header.First(x => x.Text.Contains("Clear all")));
            bool a=lst_Header[no].Displayed;

            //Config.driver.FindElement(By.XPath("//span[.='Clear all']")).Click();
            bool b = Config.driver.FindElement(By.LinkText("CLICK TO VIEW ALL PRODUCTS")).Enabled;
            bool c = Config.driver.FindElement(By.LinkText("CLICK TO VIEW ALL PRODUCTS")).Displayed;
            //driver.findelement(By.LinkText())
            //driver.findelement(By.PartialLinkText()) 


            //6. Verify default value in SortByDropDown
            Assert.IsTrue(CommonFunctions.compareDropDownDefaultSelectedValue(SearchResultsPage.dd_SortBy, SortByDropDownDefaultValue, "Comparision of dropdown default selected value"));



            //SearchResultsPage.btn_ClickToViewAllProducts.MouseOver();
            Archive.WaitForElement(By.XPath("//a[.='Click to View All Products']"));

            //Config.driver.FindElement(By.ClassName("product-grid__actions")).Click();

            //7.Select option "Low to high" in dropdown

            IWebElement ele_dd_SortBy = SearchResultsPage.dd_SortBy;
            SelectElement sel = new SelectElement(ele_dd_SortBy);
            sel.SelectByText(PriceLowToHigh);

            //ele_dd_SortBy.SelectDropDownValueByText(PriceLowToHigh);

            //Thread.Sleep(5000);

            //8.verify "Low to high order in search results"

            Assert.IsTrue(SearchResultsPage.searchResultOrderVerification(SearchResultsPage.productPriceCount, true, false));

            //9.Select option "High to low" in dropdown

            sel.SelectByText(PriceHighToLow);
            

            //verify the search results regarding to "High to low"

            Assert.IsTrue(SearchResultsPage.searchResultOrderVerification(SearchResultsPage.productPriceCount, false, true, false));

            //Select option "AlphabeticalAToZ"

            sel.SelectByText(AlphabeticalAToZ);

            //verify the search results regarding to "AlphabeticalAToZ"

            Assert.IsTrue(SearchResultsPage.searchResultOrderVerification(SearchResultsPage.title_ProductGridItem, false, false, true));

            //Verify presence of "Narrow By:" filter next to product list

            Assert.IsTrue(SearchResultsPage.verifyElementDisplayedInLeftSide(SearchResultsPage.fltr_NarrowByHeader, SearchResultsPage.img_ProductList));

            //10,19.Verify the filter and its option


            Assert.IsTrue(SearchResultsPage.verifyFilterOptions(SearchResultsPage.filter_Title, SearchResultsPage.filter_Options), "FAILED: Filter and its option verification");

            Console.WriteLine("PASSED: Filter and its option verification");

            //11.Verify the filter checkbox unchecked by default

            Assert.IsTrue(CommonFunctions.verifyCheckboxStatusAsUnChecked(SearchResultsPage.chk_FilterOptions));


            //"Product count" assert

            List<IWebElement> lst_ProductCount = Config.driver.FindElements(By.XPath(SearchResultsPage.searchResultProductCount)).ToList();

            
            Assert.IsTrue(lst_ProductCount.Count.Equals(10), "FAILED:Product count in search results page validation");
            Console.WriteLine("PASSED:Product count in search results page validation");




            //"Click to view all products" assert

            //SearchResultsPage.btn_ClickToViewAllProducts.MouseOver();
            //Archive.WaitForElement(By.LinkText("Click to View All Products"));
           




            //Assert.IsTrue(CommonFunctions.verifyElement(SearchResultsPage.btn_ClickToViewAllProducts), "FAILED: Click to View All Products button validation");

            Console.WriteLine("PASSED: Click to View All Products button validation");

           
            //js.ExecuteScript("window.scrollTo(3000, 0);");

            //12. Filter selection and its corresponding validation

            Assert.IsTrue(SearchResultsPage.filterOptionClick(SearchResultsPage.chk_FilterOptions, PriceFilterValue), "FAILED: Filter checkbox check");
            Console.WriteLine("PASSED: Filter checkbox check");

            //outcome of filter selection assertion



            Assert.IsTrue(SearchResultsPage.verifyProductPriceBasedOnFilterInSearchResultspage(SearchResultsPage.productPriceCount, PriceFilterValue), "FAILED: Product price validation based on the filter option");
            Console.WriteLine("PASSED:Product price validation based on the filter option");


            //13.product count validation

            //List<IWebElement> lstFilterContents = Config.driver.FindElements(By.XPath(SearchResultsPage.chk_FilterOptions)).ToList();                                       
            int filterOption = SearchResultsPage.chk_FilterOptions.IndexOf(SearchResultsPage.chk_FilterOptions.First(x => x.Text.Contains(PriceFilterValue)));
            //List<IWebElement> ele_ProductPriceOption1 = SearchResultsPage.chk_FilterOptions[filterOption].FindElements(By.XPath("/*")).ToList();
            //int expectedIndex = ele_ProductPriceOption1.IndexOf(ele_ProductPriceOption1.First(x => x.GetAttribute("class").Contains("facet-item__count")));


            String strExpectedOld = Config.driver.FindElement(By.XPath("//label[@for='<<REPLACECONTENT>>']/span".Replace("<<REPLACECONTENT>>", PriceFilterValue))).Text;
            //label[@for='$500-$750']/span

            String strExpected = strExpectedOld.Replace("(", "").Replace(")", "");
            IWebElement ele_ProductCountFooter = SearchResultsPage.footer_ProductCount;
            string[] strSplit = ele_ProductCountFooter.Text.Split('f');
            String strActual = strSplit[1].Trim();
            Assert.AreEqual(strExpected, strActual, "FAILED: Product count validation");
            Console.WriteLine("PASSED: Product count validation");

            //14.Filter validation with second option "Reclining" 

            Assert.IsTrue(SearchResultsPage.filterOptionClick(SearchResultsPage.chk_FilterOptions, "Reclining"), "FAILED: Filter checkbox check");
            Console.WriteLine("PASSED: Filter checkbox check");



            //Product list with "Reclining" assert

            //Thread.Sleep(3000);

            for (int m = 0; m < SearchResultsPage.title_ProductGridItem.Count; m++)
            {
                Assert.IsTrue(SearchResultsPage.title_ProductGridItem[m].Text.Contains("Reclining"), "FAILED: Product results validation based on filter option");
            }
            Console.WriteLine("PASSED:Product results validation based on filter option");


            //15.Verify "Clear all" option

            Assert.IsTrue(CommonFunctions.verifyElement(SearchResultsPage.optn_ClearAll), "FAILED: Clear all option verification");
            Console.WriteLine("PASSED: Clear all option verification");

            //16.Click on "Clear all" option



            SearchResultsPage.optn_ClearAll.Click();
           
            Console.WriteLine("PASSED: Clear all option verification");
            Assert.IsTrue(CommonFunctions.verifyCheckboxStatusAsUnChecked(SearchResultsPage.chk_FilterOptions));

            //17,18. Filter expand and collapse option validation


            //List<IWebElement> lst_filter_Title = Config.driver.FindElements(By.XPath(SearchResultsPage.filter_Title)).ToList();
            SearchResultsPage.filter_Title[0].Click();
            //Thread.Sleep(3000);
            //List<IWebElement> lst_filter_Options = Config.driver.FindElements(By.XPath(SearchResultsPage.filter_Options)).ToList(); 
            Assert.IsFalse(SearchResultsPage.filter_Title.Count.Equals(SearchResultsPage.filter_Options.Count), "FAILED: Filter collapse validation");
            Console.WriteLine("PASSED: Filter collapse validation");
            SearchResultsPage.filter_Title[0].Click();
            //Thread.Sleep(3000);
           // List<IWebElement> lst_filter_OptionsNew = Config.driver.FindElements(By.XPath(SearchResultsPage.filter_Options)).ToList();
            Assert.IsTrue(SearchResultsPage.filter_Title.Count.Equals(SearchResultsPage.filter_Options.Count), "FAILED: Filter expand validation");
            Console.WriteLine("PASSED: Filter expand validation");

            


        }

        [TestInitialize]
        public void TestStartup()
        {
            
            Archive.SetConfig(TestContext);
            Archive.LaunchDriver();
            IWebDriver a = Config.driver;
            HomePage = new HomePage();
            SearchResultsPage = new SearchResultsPage();
            CommonFunctions = new CommonFunctions();
            
            //Config.driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(10));
        }

        [TestCleanup]
        public void TestCleanup()
        {
            
            //TestContext.TestDisposal();
            Config.driver.Quit();
        }


        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;
    }
}

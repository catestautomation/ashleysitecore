﻿
using AshleyAutomationLibrary;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.PhantomJS;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using SiteCore_Regression_Automation.Pages;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SiteCore_Regression_Automation
{
    
   public class CommonFunctions
    {

        //public IWebDriver LaunchDriver()
        //{
        //    Archive.LaunchDriver();
        //}

        //    if (Archive.Config.remoteExecution=="True")
        //    {
        //        Config.driver = new RemoteWebDriver(ConstructCap());
        //    }
        //    else
        //    {
        //        switch (Config.browserName.ToString().TrimStart().TrimEnd().ToLower())
        //        {
        //            case "chrome":
        //                Config.driver = new ChromeDriver(Directory.GetCurrentDirectory() + @"\TestDrivers\");
        //                break;
        //            case "firefox":
        //                Config.driver = new FirefoxDriver();
        //                break;
        //            case "internetexplorer":
        //                Config.driver = new InternetExplorerDriver(Directory.GetCurrentDirectory() + @"\TestDrivers\");
        //                break;
        //            case "phantom":
        //                Config.driver = new PhantomJSDriver(Directory.GetCurrentDirectory() + @"\TestDrivers\");
        //                break;
        //            case "edge":
        //                {
        //                    Config.driver = new EdgeDriver(Directory.GetCurrentDirectory() + @"\TestDrivers\");
        //                    break;
        //                }
        //        }
        //    }

        //    Config.driver.Manage().Window.Maximize();

        //    Config.driver.Navigate().GoToUrl(Config.appUrl);
        //    return Config.driver;
        //}

        //public DesiredCapabilities ConstructCap()
        //{
        //    DesiredCapabilities cap = new DesiredCapabilities();


        //    return cap;
        //}

        //public void setConfig(TestContext context)
        //{
        //    Config.browserName = context.Properties["Browser"].ToString();
        //    Config.appUrl= context.Properties["AppUrl"].ToString();
        //    Config.platform= context.Properties["Platform"].ToString();
        //    Config.hubUrl= context.Properties["HubUrl"].ToString();
        //    Config.remoteExecution = context.Properties["RemoteExecution"].ToString();
        //}

        public static string result = string.Empty;

        public object TestContext { get; internal set; }

        public string searchForProduct(String productname, bool IsProductCodePrevious = false, bool IsCartPageSearch = false)
        {

              HomePage HomePage = new HomePage();
              CartPage CartPage = new CartPage();
            try
            {
                Thread.Sleep(3000);
                if (IsCartPageSearch == false)
                {
                    IWebElement ele_text_searchfield = HomePage.txt_searchInputField;
                    ele_text_searchfield.SendKeys(productname);
                    if (IsProductCodePrevious == false)
                    {
                        HomePage.btn_searchInputbutton.Click();
                        Archive.WaitForPageLoad();
                    }
                    else
                    {
                         HomePage.btn_searchInputbuttonOld.Click();
                        Archive.WaitForPageLoad();
                    }
                    Thread.Sleep(5000);
                }
                else
                {
                    CartPage.txt_cartPageSearchInputField.SendKeys(productname);
                    CartPage.btn_CartPageSearchInputbutton.Click();
                    Archive.WaitForPageLoad();
                    Thread.Sleep(5000);
                }
            }
            catch (Exception ex)
            {
                result = "Error :" + ex.Message + ex.StackTrace;
            }

            return result;

        }

        /// <summary>
        /// My Account Option Dropdown has been selected
        /// </summary>
        /// <param name="dropDown"></param>
        /// <returns></returns>
        public string myAccountOption(string dropDown)
        {
            try
            {
                HomePage HomePage = new HomePage();
                Header Header = new Header();
                Archive.WaitForElement(By.XPath("//div[contains(@class,'homepage')]//img[contains(@id,'accountIcon')]"));
                HomePage.img_myAccount.MouseOver();
                Archive.WaitForElement(By.Id("afhs-account-dropdown"));
                List<IWebElement> drpdwn_list = Header.drpdwn_dropDownParent.GetDescendants();
                int option_Index = drpdwn_list.IndexOf(drpdwn_list.First(x => x.Text.Contains(dropDown)));
                Thread.Sleep(3000);
                drpdwn_list[option_Index].Click();
            }
            catch (Exception ex)
            {
                result = "Error :" + ex.Message + ex.StackTrace;
            }

            return result;
        }

        public bool compareDropDownDefaultSelectedValue(IWebElement elmntDropDown, String strExpected, String strLogDescription)
        {
            bool isCompared = false;
            try
            {
                //IWebElement ele_DropDown = obj.Init(PropertyExpression.Contains);
                SelectElement sel = new SelectElement(elmntDropDown);
                String strActual = sel.SelectedOption.Text;
                isCompared = strExpected.Equals(strActual);
                Console.WriteLine("PASSED:" + strLogDescription);
            }
            catch (Exception e)
            {
                isCompared = false;
                System.Console.WriteLine(e.Message);
                Console.WriteLine("FAILED:" + strLogDescription);
            }
            return isCompared;

        }


        public bool verifyCheckboxStatusAsUnChecked(IList<IWebElement> ele_VerifyContent)
        {
            bool isChecked = false;
           // List<IWebElement> lst_FilterCheckbox = Config.driver.FindElements(By.XPath(ele_VerifyContent)).ToList();
            try
            {
                for (int z = 0; z < ele_VerifyContent.Count; z++)
                {
                    if (!ele_VerifyContent[z].Selected)
                    {
                        System.Console.WriteLine("unchecked status>>");
                        isChecked = true;
                    }
                    else
                    {
                        isChecked = false;
                        break;
                    }
                }
            }
            catch (Exception e)
            {
                System.Console.WriteLine(e.Message);
                isChecked = false;
            }
            return isChecked;
        }


        public bool verifyElement(IWebElement elmntToVerify)
        {
            bool isVerified = false;
            try
            {

                //IWebElement elmnt_ToVerify = obj.Init(PropertyExpression.Contains);
                isVerified = elmntToVerify.Enabled || elmntToVerify.Displayed;
        

            }
            catch (Exception e)
            {
                isVerified = false;
                System.Console.WriteLine(e.Message);
            }
            return isVerified;
        }


        public static bool verifyAscendingOrder(List<String> lstToBeVerified)
        {
            bool isVerified = false;
            var ordered = lstToBeVerified.OrderBy(x => x).ToList();
            System.Console.WriteLine("List is>>" + ordered);
            System.Console.WriteLine("List2 is>>" + lstToBeVerified);
            if (ordered.SequenceEqual(lstToBeVerified))
            {
                isVerified = true;
            }
            else
            {
                isVerified = false;
            }

            return isVerified;
        }

        public static bool verifyDescendingOrder(List<String> lstToBeVerified)
        {
            bool isVerified = false;
            try
            {
                var ordered = lstToBeVerified.OrderByDescending(x => x).ToList();
                System.Console.WriteLine("List is>>" + ordered);
                System.Console.WriteLine("List2 is>>" + lstToBeVerified);
                if (ordered.SequenceEqual(lstToBeVerified))
                {
                    isVerified = true;
                }
                else
                {
                    isVerified = false;
                }
            }
            catch (Exception e)
            {
                System.Console.WriteLine(e.Message);
                isVerified = false;
            }
            return isVerified;
        }


    }
}

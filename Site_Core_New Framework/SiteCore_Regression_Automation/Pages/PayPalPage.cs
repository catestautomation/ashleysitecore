﻿using AshleyAutomationLibrary;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteCore_Regression_Automation.Pages
{
    public class PayPalPage
    {
        public PayPalPage()
        {
            PageFactory.InitElements(Config.driver, this);
        }
    
        [FindsBy(How = How.Id, Using = "email")]
        public IWebElement txt_payPalEmail;

        [FindsBy(How = How.Id, Using = "password")]
        public IWebElement txt_payPalPassword;

        [FindsBy(How = How.Id, Using = "btnLogin")]
        public IWebElement btn_payPalLoginButton;

        [FindsBy(How = How.Id, Using = "confirmButtonTop")]
        public IWebElement btn_payPalContinueButton;


    }
}

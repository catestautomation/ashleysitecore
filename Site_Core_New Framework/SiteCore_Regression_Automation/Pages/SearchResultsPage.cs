﻿
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OpenQA.Selenium.Support.PageObjects;
using SiteCore_Regression_Automation;
using AshleyAutomationLibrary;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SiteCore_Regression_Automation.Pages
{
    public class SearchResultsPage
    {
        public string result = string.Empty;
        public SearchResultsPage()
        {
            PageFactory.InitElements(Config.driver, this);
        }

        [FindsBy(How = How.LinkText, Using = "My Account")]
        public IWebElement ele_myAccount;


        [FindsBy(How = How.Id, Using = "USI_toolbar_all")]
        public IWebElement ele_BottomOverlayClose;

        [FindsBy(How = How.Id, Using = "imgChatTop2")]
        public IWebElement ele_LaunchOverlayClose;

        [FindsBy(How = How.TagName, Using = "h1")]
        public IWebElement txt_headig;

        [FindsBy(How = How.Id, Using = "resultsContainer_products")]
        public IWebElement con_results;

        [FindsBy(How = How.ClassName, Using = "product-grid__list")]
        public IWebElement con_subCategoryResults;

        [FindsBy(How = How.ClassName, Using = "product-grid__footer")]
        public IWebElement con_resultCountInfo;

        [FindsBy(How = How.Id, Using = "TbxSearch")]
        public IWebElement txt_searchInputField;

        [FindsBy(How = How.Id, Using = "ctl09_searchIcon2")]
        public IWebElement btn_searchInputButton;

        [FindsBy(How = How.ClassName, Using = "product-more-options")]
        public IWebElement btn_productmoreOption;

        [FindsBy(How = How.Id, Using = "checkPriceZipCode")]
        public IWebElement txt_zipCodeSideBar;

        [FindsBy(How = How.ClassName, Using = "overlay__container")]
        public IWebElement con_zipcodeContainer;

        [FindsBy(How = How.Id, Using = "quickViewCheckPriceModal")]
        public IWebElement con_quickViewZipCodeContainer;

        [FindsBy(How = How.Id, Using = "addToCart")]
        public IWebElement con_productLightBox;

        [FindsBy(How = How.Id, Using = "addToCartQuickView")]
        public IWebElement btn_addToCartButtonQuickView;

        [FindsBy(How = How.ClassName, Using = "accordion subCategory facets_container")]
        public IWebElement con_leftSidePriceFilterContainer;

        [FindsBy(How = How.Id, Using = "price-filterr")]
        public IWebElement fltr_leftSidepriceFilter;

        [FindsBy(How = How.Id, Using = "clear_all_selected_facets")]
        public IWebElement btn_clearAll;

        [FindsBy(How = How.Id, Using = "productsSortBy")]
        public IWebElement dd_sortByDropdown;

        public String String_dd_sortByDropdown = "productsSortBy";

        [FindsBy(How = How.ClassName, Using = "product-facets__header")]
        public IWebElement btn_subCategoryClearAllParent;

        [FindsBy(How = How.ClassName, Using = "logo")]
        public IWebElement img_AshleyLogo;

        [FindsBy(How = How.ClassName, Using = "product-price")]
        public IList<IWebElement> div_productPrice;

        [FindsBy(How = How.ClassName, Using = "product-price__check-price")]
        public IWebElement lnk_checkPrice;

        [FindsBy(How = How.ClassName, Using = "zip-code-overlay__input")]
        public IWebElement txt_zipCodeOverlay;

        [FindsBy(How = How.XPath, Using = "//button[contains(@class,'zip-code-overlay__button')]")]
        public IWebElement btn_zipCodeOverlayButton;

        [FindsBy(How = How.ClassName, Using = "product-swatches")]
        public IList<IWebElement> con_productSwatch;

        [FindsBy(How = How.LinkText, Using = "To View Local Pricing")]
        public IWebElement lnk_ToViewLocalPricing;

        [FindsBy(How = How.ClassName, Using = "product-zip-code__content-link")]
        public IWebElement lnk_EnterYourZIPCode;

        [FindsBy(How = How.ClassName, Using = "product-grid-item__content")]
        public IWebElement searchResultCount;

        [FindsByAll]
        [FindsBy(How = How.XPath, Using = "//span[contains(@class,'product-price') and contains(.,'$')]")]
        public IList<IWebElement> productPriceCount;
      
        [FindsBy(How = How.Id, Using = "productsSortBy")]
        public IWebElement dd_SortBy;

        [FindsBy(How = How.TagName, Using = "svg")]
        public IWebElement spinner_searchResultsPage;

        [FindsByAll]
        [FindsBy(How = How.ClassName, Using = "product-grid-item__title")]
        public IList<IWebElement> title_ProductGridItem;

        [FindsBy(How = How.ClassName, Using = "product-grid__list")]
        public IWebElement img_ProductList;

        [FindsBy(How = How.ClassName, Using = "product-facets__title")]
        public IWebElement fltr_NarrowByHeader;

        [FindsBy(How = How.ClassName, Using = "product-facets")]
        public IWebElement con_Filter;

        [FindsByAll]
        [FindsBy(How = How.ClassName, Using = "facet-item__link")]
        public IList<IWebElement> chk_FilterOptions;

        public String searchResultProductCount = "//a[@class='product-grid-item__link']";

        [FindsBy(How = How.PartialLinkText, Using = "Click to View All Products")]
        public IWebElement btn_ClickToViewAllProducts;

        [FindsBy(How = How.LinkText, Using = "Sign In")]
        public IWebElement btn_SignIn;

        [FindsBy(How = How.Id, Using = "ctl10_accountIcon")]
        public IWebElement btn_MyAccount;
  
        [FindsByAll]
        [FindsBy(How = How.TagName, Using = "span",Priority =1)]
        [FindsBy(How = How.ClassName, Using = "product-facets__clear-all", Priority=0)]
        public IWebElement optn_ClearAll;

        [FindsBy(How = How.ClassName, Using = "product-grid-item__shipping-label")]
        public IWebElement lbl_ProductShippingOption;

        [FindsBy(How = How.PartialLinkText, Using = "$250-$500")]
        public IWebElement chk_ProductPriceOption1;
        
        [FindsBy(How = How.Id, Using = "$250-$500")]
        public IWebElement chk_ProductOption;

        [FindsByAll]
        [FindsBy(How = How.ClassName, Using = "facet-group__list")]
        public IList<IWebElement> filter_Options;

        [FindsByAll]
        [FindsBy(How = How.ClassName, Using = "facet-group__title")]
        public IList<IWebElement> filter_Title;

        [FindsBy(How = How.ClassName, Using = "product-grid__showing-items")]
        public IWebElement footer_ProductCount;

        [FindsBy(How = How.Id, Using = "tab_products")]
        public IWebElement productCountInHeader;

        [FindsBy(How = How.PartialLinkText, Using = "Search Results For \"Stool\"")]
        public IWebElement searchResultInformation;

        [FindsBy(How = How.ClassName, Using = "prodThumbResultCountTop displayPrdCount")]
        public IWebElement ProductCountFromSearchInformation;


        /// <summary>
        /// For filter options click in SearchResultsPage
        /// </summary>
        /// <param name="lstFilterOptions"></param>
        /// <param name="strFilterOption"></param>
        /// <returns></returns>
        public bool filterOptionClick(IList<IWebElement> lstFilterOptions, String strFilterOption)
        {
            bool isClicked = false;
            try
            {
                int filterOption = lstFilterOptions.IndexOf(lstFilterOptions.First(x => x.Text.Contains(strFilterOption)));
                lstFilterOptions[filterOption].MouseOver();
                lstFilterOptions[filterOption].Click();
                isClicked = true;
            }
            catch (Exception e)
            {
                System.Console.WriteLine(e.Message);
                isClicked = false;
            }
            return isClicked;
        }



        public bool verifyFilterOptions(IList<IWebElement> lst_filter_Title, IList<IWebElement> lst_filter_Options)
        {
            bool isVerified = false;

            try
            {
                if (lst_filter_Title.Count == lst_filter_Options.Count)
                {
                    isVerified = true;
                }

            }
            catch (Exception e)
            {
                isVerified = false;
                System.Console.WriteLine(e.Message);
            }
            return isVerified;
        }

        /// <summary>
        /// for searching the product with amount in "$" in searchResultsPage
        /// </summary>
        /// <param name="lst_searchResultCount"></param>
        /// <param name="lst_productPriceCount"></param>
        /// <param name="srDescription"></param>
        /// <returns></returns>
        public bool verifyProductPriceInProductList(IList<IWebElement> lst_searchResultCount, IList<IWebElement> lst_productPriceCount, String srDescription)
        {
            bool isVerified = false;
            try
            {
                if (lst_productPriceCount.Count == lst_searchResultCount.Count)
                {
                    isVerified = true;
                    //WebArchive.WriteLog("PASSED:" + srDescription);
                }
                else
                {
                    isVerified = false;
                    //WebArchive.WriteLog("FAILED:" + srDescription);
                }
            }
            catch (Exception e)
            {
                isVerified = false;
                //WebArchive.WriteLog("FAILED:" + srDescription);
                Console.Out.WriteLine(e.Message);
            }
            return isVerified;
        }

        /// <summary>
        /// Enter ZIPCode in ZIPCode popup
        /// </summary>
        /// <param name="strZIPCode"></param>
        /// <param name="strLogDescription"></param>
        /// <returns></returns>
        public bool enterZIPCodeInSearchResultsPageZIPCodePopUp(String strZIPCode, String strLogDescription)
        {
            bool isEntered = false;
            try
            {
                txt_zipCodeOverlay.SendKeys(strZIPCode);
                btn_zipCodeOverlayButton.Click();
                isEntered = true;
            }
            catch (Exception e)
            {
                isEntered = false;
                System.Console.WriteLine(e.Message);
                System.Console.WriteLine("FAILED:" + strLogDescription);
            }

            return isEntered;
        }


        /// <summary>
        /// Search product from SearchResultsPage
        /// </summary>
        /// <param name="IsQuickView"></param>
        /// <param name="IsSearchResults"></param>
        /// <returns></returns>
        public string selectProductFromSearchResultsPage(bool IsQuickView = false, bool IsSearchResults = false)
        {
            try
            {
                if (IsSearchResults == false)
                {
                    Archive.WaitForElement(By.ClassName("product-grid__list"));
                    List<IWebElement> list_Products = con_subCategoryResults.FindElements(By.ClassName("aspect-ratio-image__image")).ToList();
                    if (IsQuickView == false)
                    {
                        list_Products[0].Click();
                        Archive.WaitForPageLoad();
                    }
                    else
                    {
                        list_Products[0].MouseOver();
                        List<IWebElement> quickViewlist_Products = con_subCategoryResults.FindElements(By.ClassName("product-grid-item__image-button")).ToList();
                        quickViewlist_Products[0].Click();
                        Archive.WaitForPageLoad();
                    }
                }
                else
                {
                    Archive.WaitForElement(By.Id("resultsContainer_products"));
                    List<IWebElement> list_Products = con_results.FindElements(By.ClassName("thumbnailImage")).ToList();
                    if (IsQuickView == false)
                    {
                        list_Products[0].Click();
                        Archive.WaitForPageLoad();
                    }
                    else
                    {
                        list_Products[0].MouseOver();
                        List<IWebElement> quickViewlist_Products = con_results.FindElements(By.ClassName("button secondary tiny quickView")).ToList();
                        quickViewlist_Products[0].Click();
                        Archive.WaitForPageLoad();
                    }
                }
                result = "Selecting Product from search reults page is Succesful - QuickView selection - " + IsQuickView;
            }
            catch (Exception ex)
            {
                result = "Failure in Selecting Product from search reults page - QuickView selection" + IsQuickView + ex.Message + ex.StackTrace;
            }
            return result;
        }


        /// <summary>
        /// check price click in product SearchResultsPage
        /// </summary>
        /// <param name="zipCode"></param>
        /// <returns></returns>
        public string checkPriceSearchResultsPage(string zipCode)
        {
            SearchResultsPage SearchResultsPage = new SearchResultsPage();
            try
            {
                int priceCount = div_productPrice.Count;
                for (int i = 0; i < priceCount; i++)
                {
                    List<IWebElement> productPriceChildren = div_productPrice[i].GetDescendants();
                    if (lnk_checkPrice.Text == "Check price")
                    {
                        lnk_checkPrice.Click();
                        Archive.WaitForPageLoad();
                        break;
                    }
                }
                txt_zipCodeOverlay.SendKeys(zipCode);
                btn_zipCodeOverlayButton.Click();
                Archive.WaitForPageLoad();
                result = "Clicking check price and entering Zipcode in search results page is succesful";
            }
            catch (Exception ex)
            {
                result = "Failure in selecting check price and entering Zipcode in search results page" + ex.Message + ex.StackTrace;
            }

            return result;

        }


        /// <summary>
        /// verify Color swatch in SearchResultsPage
        /// </summary>
        /// <returns></returns>
        public string colorSwatchInSearchResultsPage()
        {
            try
            {
                List<IWebElement> colorSwatch = con_productSwatch[0].FindElements(By.TagName("img")).ToList();
                colorSwatch[0].Click();
                Archive.WaitForPageLoad();
            }
            catch (Exception ex)
            {
                result = "Error :" + ex.Message + ex.StackTrace;
            }
            return result;
        }


        /// <summary>
        /// Enter ZIPCode under "ToViewAllLocalPricing"
        /// </summary>
        /// <returns></returns>
        public bool clickEnterZipCodeUnderToViewAllLocalPricing()
        {
            bool isClicked = false;
            try
            {
                Archive.WaitForPageLoad();
                lnk_EnterYourZIPCode.MouseOver();
                lnk_EnterYourZIPCode.Click();
                isClicked = true;

            }
            catch (Exception e)
            {
                System.Console.WriteLine(e.Message);
                isClicked = false;
            }
            return isClicked;
        }


        /// <summary>
        /// Verify the order of the product list based on the dropdown
        /// </summary>
        /// <param name="ele_ProductCount"></param>
        /// <param name="IsAscendingOrder"></param>
        /// <param name="IsDescendingOrder"></param>
        /// <param name="isAlphabeticalOrder"></param>
        /// <returns></returns>
        public bool searchResultOrderVerification(IList<IWebElement> ele_ProductCount, bool IsAscendingOrder = false, bool IsDescendingOrder = false, bool isAlphabeticalOrder = false)
        {
            bool isVerified = false;
            try
            {
                Thread.Sleep(3000);
                Archive.WaitForElement(By.Id("productsSortBy"));
                List<String> lstProductFinalList = new List<String>();
                for (int i = 0; i < ele_ProductCount.Count; i++)
                {
                    if (ele_ProductCount[i].GetAttribute("class").Contains("old"))
                    {
                        System.Console.WriteLine("Skipped>>" + ele_ProductCount[i].Text);
                    }
                    else
                    {
                        lstProductFinalList.Add(ele_ProductCount[i].Text);
                        System.Console.WriteLine("The data is>>" + ele_ProductCount[i].Text);
                    }
                }
                if ((IsAscendingOrder == true) || (isAlphabeticalOrder == true))
                {
                    isVerified = CommonFunctions.verifyAscendingOrder(lstProductFinalList);
                }
                else if (IsDescendingOrder == true)
                {
                    isVerified = CommonFunctions.verifyDescendingOrder(lstProductFinalList);
                }
            }
            catch (Exception e)
            {
                System.Console.WriteLine(e.Message);
                isVerified = false;
            }
            return isVerified;
        }

        /// <summary>
        /// Verify element displayed in the leftside
        /// </summary>
        /// <param name="elmntOne"></param>
        /// <param name="elmntTwo"></param>
        /// <returns></returns>
        public bool verifyElementDisplayedInLeftSide( IWebElement elmntOne, IWebElement elmntTwo)
        {
            bool isVerified = false;
           
            try
            {
                System.Console.WriteLine(elmntOne.Location.X);
                System.Console.WriteLine(elmntTwo.Location.X);
                if ((elmntOne.Location.X) < (elmntTwo.Location.X))
                {
                    isVerified = true;
                }
                else
                {
                    isVerified = false;
                }
            }
            catch (Exception e)
            {
                isVerified = false;
                System.Console.WriteLine(e.Message);
            }
            return isVerified;
        }

        /// <summary>
        /// Verify product price based on the filter in SearchResultsPage
        /// </summary>
        /// <param name="lstProductList"></param>
        /// <param name="producePriceFilterOption"></param>
        /// <returns></returns>
        public bool verifyProductPriceBasedOnFilterInSearchResultspage(IList<IWebElement> lstProductList, String producePriceFilterOption)
        {
            bool isVerified = false;
            try
            {
                String[] price = producePriceFilterOption.Split('-');
                List<String> lstProductFinalList = new List<String>();
                for (int i = 0; i < lstProductList.Count; i++)
                {
                    if (lstProductList[i].GetAttribute("class").Contains("old"))
                    {
                        System.Console.WriteLine("Skipped>>" + lstProductList[i].Text);
                    }
                    else
                    {
                        lstProductFinalList.Add(lstProductList[i].Text);
                        System.Console.WriteLine("The data is>>" + lstProductList[i].Text);
                    }
                }
                for (int j = 0; j < lstProductFinalList.Count; j++)
                {
                    if (float.Parse(lstProductFinalList[j].Replace("$", "").Trim()) >= float.Parse(price[0].Replace("$", "").Trim()) && float.Parse(lstProductFinalList[j].Replace("$", "").Trim()) <= float.Parse(price[1].Replace("$", "").Trim()))
                    {
                        isVerified = true;
                    }
                    else
                    {
                        isVerified = false;
                        break;
                    }
                }
            }
            catch (Exception e)
            {
                isVerified = false;
                System.Console.WriteLine(e.Message);
            }
            return isVerified;
        }
    }
}


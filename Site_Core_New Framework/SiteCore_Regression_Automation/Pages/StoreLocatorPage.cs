﻿using AshleyAutomationLibrary;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteCore_Regression_Automation.Pages
{
    public class StoreLocatorPage
    {
        public StoreLocatorPage()
        {
            PageFactory.InitElements(Config.driver, this);
        }

        
        [FindsBy(How = How.Id, Using = "FindStoreTab")]
        public IWebElement dd_findAStore;

        [FindsBy(How = How.Id, Using = "StoreLocationListTab")]
        public IWebElement dd_storeLocationList;

        [FindsBy(How = How.ClassName, Using = "locationContent")]
        public IWebElement elm_locationInformation;

        [FindsBy(How = How.ClassName, Using = "distantContent")]
        public IWebElement elm_distanceInfo;
        
        [FindsBy(How = How.ClassName, Using = "selectCountryContent")]
        public IWebElement elm_countryInfo;

        [FindsBy(How = How.Id, Using = "FindStore")]
        public IWebElement elm_results;

        [FindsBy(How = How.ClassName, Using = "main")]
        public IWebElement elm_relatedSearchResults;

        [FindsBy(How = How.Id, Using = "map_canvas")]
        public IWebElement elm_map;

        [FindsBy(How = How.ClassName, Using = "searchResults")]
        public IWebElement elm_storeDetailsParent;

        [FindsBy(How = How.Id, Using = "descriptionStore")]
        public IWebElement elm_shopDetails;

        [FindsBy(How = How.Id, Using = "contentGoogleMaps")]
        public IWebElement elm_mapAtShopDetailPage;

        [FindsBy(How = How.ClassName, Using = "banner")]
        public IWebElement elm_banner;

        [FindsBy(How = How.Id, Using = "StoreLocationError")]
        public IWebElement elm_errorText;

        [FindsBy(How = How.Id, Using = "TbxLocation")]
        public IWebElement txt_location;

        [FindsBy(How = How.Id, Using = "Tbxdistance")]
        public IWebElement dd_distance;

        [FindsBy(How = How.Id, Using = "Tbxcountry")]
        public IWebElement dd_country;

        [FindsBy(How = How.ClassName, Using = "resultsForcountry")]
        public IWebElement elm_homeStoreResults;

        [FindsBy(How = How.Id, Using = "slc")]
        public IWebElement dd_storeLocationCountry;

    }
}

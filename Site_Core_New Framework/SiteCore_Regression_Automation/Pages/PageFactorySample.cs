﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ashley.QA.Automation.SiteCore.Pages
{
    class PageFactorySample
    {
        private IWebDriver driver;

        public PageFactorySample(IWebDriver driver)
        {
            this.driver = driver;
            PageFactory.InitElements(driver, this);
        }

        [FindsBy(How = How.LinkText, Using = "My Account")]
        public static IWebElement ele_myAccount;

        [FindsBy(How = How.XPath,Priority=1, Using="//span[.='My Account']")]
        [FindsBy(How = How.XPath, Priority = 2, Using = "//span[.='abcd']")]
        public static IWebElement ele_myAccount1;

        public static String strMyAccountLable = "//span[.='My Account']";

        public static String siteCoreUrl = "http://ashcomm7.cds.ashleyretail.com/";





    }
}

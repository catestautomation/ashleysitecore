﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium;
using AshleyAutomationLibrary;
using System.Threading;

namespace SiteCore_Regression_Automation.Pages
{

    public class HomePage
    {
        private string strReplaceContent;
        public string result = string.Empty;
        //        IWebDriver driver;



        public HomePage()
        {
            PageFactory.InitElements(Config.driver, this);

        }

        [FindsBy(How = How.XPath, Using = "//ul[@id='afhs-menu']")]
        public IWebElement ele_Check;



        [FindsBy(How = How.XPath, Using = "//div[@class='afhs-account-search-container']")]
        public IWebElement ele_Check12;



        [FindsBy(How = How.XPath, Using = "//li[@class='afhs-mega-dropdown has-dropdown not-click']/a[.='Furniture']")]
        public IWebElement furnitureCheckUp;

        public String strFurnitureCheckUp = "//li[@class='afhs-mega-dropdown has-dropdown not-click']/a[.='Furniture']";

        [FindsBy(How = How.Id, Using = "ctl09_accountIcon")]   //Ashcomm5
        [FindsBy(How = How.Id, Using = "ctl10_accountIcon")]   //Ashcomm9
        public IWebElement img_myAccount;

        [FindsBy(How = How.LinkText, Using = "My Account")]
        public IWebElement ele_myAccount;

        [FindsBy(How = How.ClassName, Using = ".spinner")]
        public IWebElement ajax_Loader;

        [FindsBy(How = How.Id, Using = "ctl09_afhsLogoLink")]
        public IWebElement icn_ashleyLogo;

        [FindsBy(How = How.ClassName, Using = "successPopupReject")]
        public IWebElement lnk_noThankyou;

        [FindsBy(How = How.Id, Using = "emailOverlayClose")]
        public IWebElement btn_noThankyouClose;

        ///public object img_myAccount = new { Id = strReplaceContent };
        [FindsBy(How = How.ClassName, Using = "afhs-my-account-link")]
        public IWebElement lnk_myAccount;

        [FindsBy(How = How.ClassName, Using = "afhs-mega-nav")]
        public IWebElement mnu_category;

        [FindsBy(How = How.ClassName, Using = "afhs-mega-dropdown has-dropdown not-click")]
        public IList<IWebElement> mnu_categoryList;

        [FindsBy(How = How.ClassName, Using = "mega-menu")]
        public IWebElement mnu_dropdown;

        [FindsBy(How = How.Id, Using = "TbxSearch")]
        public IWebElement txt_searchInputField;//AshComm6

        //public object txt_searchInputField = new { Class = "search-box__input" };
        // public object btn_searchInputbutton = new { Id = "ctl10_searchIcon2" };//

        [FindsBy(How = How.ClassName, Using = "search-box__icon")]
        public IWebElement btn_searchInputbutton;



        [FindsBy(How = How.XPath, Using = "//div[@id='afhsUnbxd']/descendant::img[contains(@id,'searchIcon')]")]
        public IWebElement btn_searchInputbuttonOld;

        //[FindsBy(How = How.Custom, Using = "searchIcon")]
        //[CacheLookup]
        //public IWebElement btn_searchInputbuttonOld1;

        /// <summary>
            /// Subcategory has been slected by mousehovering in category
            /// </summary>
            /// <param name="Category"></param>
            /// <param name="SubCategory"></param>
            /// <returns></returns>
        public string subCategorySelection(string Category, string SubCategory)
        {

            try
            {                
                //IWebElement ele_mnu_category = HomePage.mnu_category.Init();
                Thread.Sleep(5000);
                List<IWebElement> ele_mnu_categoryList = mnu_category.FindElements(By.ClassName("dropdown")).ToList();
                int categoryCount = ele_mnu_categoryList.Count();
                List<IWebElement> category_lists = mnu_category.FindElements(By.ClassName("show-js")).ToList();
                int option_Index = category_lists.IndexOf(category_lists.First(x => x.Text.Contains(Category)));
                category_lists[option_Index].MouseOver();

                List<IWebElement> subCategoryList = ele_mnu_categoryList[option_Index].FindElements(By.TagName("a")).ToList();
                option_Index = subCategoryList.IndexOf(subCategoryList.First(x => x.Text.Contains(SubCategory)));
                Thread.Sleep(3000);
                subCategoryList[option_Index].Click();

                result = Category + "and" + SubCategory + "selection has been performed succesfully";

            }
            catch (Exception ex)
            {

                result = "Failure in selecting the" + Category + "and" + SubCategory + ex.Message + ex.StackTrace;
            }

            return result;

        }

    }
}





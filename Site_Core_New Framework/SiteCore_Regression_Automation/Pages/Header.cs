﻿using AshleyAutomationLibrary;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteCore_Regression_Automation.Pages
{
    public class Header
    {
        public Header()
        {
            PageFactory.InitElements(Config.driver, this);
        }

        [FindsBy(How = How.Id, Using = "ctl09_afhsLogoLink")]
        public IWebElement img_ashleyLogo;

        [FindsBy(How = How.Id, Using = "TbxSearch")]
        public IWebElement txt_searchInputField;

        [FindsBy(How = How.ClassName, Using = "search-box")]
        public IWebElement elm_subCategorySearchBox;

        [FindsBy(How = How.Id, Using = "ctl09_searchIcon2")]
        public IWebElement btn_searchInputButton;

        [FindsBy(How = How.Id, Using = "ctl09_cartIcon")]
        public IWebElement img_cartIcon;

        [FindsBy(How = How.Id, Using = "afhs-account-dropdown")]
        public IWebElement drpdwn_dropDownParent;

        [FindsBy(How = How.TagName, Using = "a")]
        public IWebElement drpdwn_lists;

        [FindsBy(How = How.ClassName, Using = "menu-dropdown__item")]
        public IWebElement dropdwn_list;
        
        [FindsBy(How = How.ClassName, Using = "afhs-top-menu")]
        public IWebElement elm_topMenu;

        [FindsBy(How = How.ClassName, Using = "unbxd-as-wrapper unbxd-as-extra-right")]
        public IWebElement elm_searchSuggestionDDL;

        [FindsBy(How = How.PartialLinkText, Using = "Help")]
        public IWebElement lnk_Help;

        [FindsBy(How = How.ClassName, Using = "afhs-logo-img")]
        public IWebElement icn_AshleyHomeStore;




    }
}

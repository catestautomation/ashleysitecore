﻿using AshleyAutomationLibrary;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteCore_Regression_Automation.Pages
{
    public class OrderDetailsPage
    {
        public OrderDetailsPage()
        {
            PageFactory.InitElements(Config.driver, this);
        }

        [FindsBy(How = How.Id, Using = "scphbody_0_UnregisteredOrder_TbxConfirmationNumber")]
        public IWebElement txt_confirmationNumber;

        [FindsBy(How = How.Id, Using = "scphbody_0_UnregisteredOrder_TbxOrderEmail")]
        public IWebElement txt_emailAddress;

        [FindsBy(How = How.Id, Using = "scphbody_0_UnregisteredOrder_TbxBillingZip")]
        public IWebElement txt_billingZipCode;

        [FindsBy(How = How.Id, Using = "OrderDetailsForm")]
        public IWebElement con_orderDetailsForm;

        [FindsBy(How = How.Id, Using = "scphbody_0_UnregisteredOrder_ViewOrder")]
        public IWebElement btn_reviewOrder;

        [FindsBy(How = How.ClassName, Using = "side-nav-icon order-history")]
        public IWebElement lnk_orderHistory;

        //public static object lnk_recentOrders = new { TagName = "a", Text = "Recent Orders" };
        [FindsBy(How = How.PartialLinkText, Using = "Recent Orders")]
        public IWebElement lnk_recentOrders;


    }
        
        
}

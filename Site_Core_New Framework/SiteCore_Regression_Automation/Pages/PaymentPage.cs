﻿using AshleyAutomationLibrary;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SiteCore_Regression_Automation.Pages
{
    public class PaymentPage
    {
        public PaymentPage()
        {
            PageFactory.InitElements(Config.driver, this);
        }
        public static string result = string.Empty;
        
        [FindsBy(How = How.Id, Using = "CCard_CNber")]
        public IWebElement txt_cardNumber;

        [FindsBy(How = How.Id, Using = "CCard_SecurityCode")]
        public IWebElement txt_securityCode;

        [FindsBy(How = How.Id, Using = "CCard_CardHolderFirstName")]
        public IWebElement txt_firstNameOnCard;

        [FindsBy(How = How.Id, Using = "CCard_CardHolderLastName")]
        public IWebElement txt_lastNameOnCard;

        [FindsBy(How = How.Id, Using = "CCard_ExpireMonth")]
        public IWebElement txt_expMonthOnCard;

        [FindsBy(How = How.Id, Using = "CCard_ExpireYear")]
        public IWebElement txt_expYearOnCard;

        [FindsBy(How = How.Id, Using = "confirm-submission")]
        public IWebElement btn_continueButtonPayment;

        //public static object txt_ashleyCardNumber = new { Id = "advantageCardNumber" };

        [FindsBy(How = How.ClassName, Using = "financing-options")]
        public IWebElement rdbtn_FinancingOptionsParent;

        [FindsBy(How = How.Id, Using = "view-financing-terms")]
        public IWebElement btn_viewFinancingTerm;

        [FindsBy(How = How.Id, Using = "AshleyAdvantageCard_AccountNumber")]
        public IWebElement txt_ashleyCardNumber;

        [FindsBy(How = How.Id, Using = "advantage-card")]
        public IWebElement txt_ashleyAdvantageCard;

        [FindsByAll]
        [FindsBy(How = How.ClassName, Using = "radio-option")]
        public IList<IWebElement> con_radioButton;

        [FindsBy(How = How.Id, Using = "accept-terms-button")]
        public IWebElement btn_termsOverlayAccept;

        [FindsBy(How = How.Id, Using = "take-advantage-action")]
        public IWebElement lnk_takeAdvantage;

        [FindsBy(How = How.Id, Using = "pay-pal")]
        public IWebElement lnk_payPal;

        [FindsBy(How = How.ClassName, Using = "msax-DiscountTotal")]
        public IWebElement tr_salePromotion;

        [FindsBy(How = How.Id, Using = "stateRecyclingFee")]
        public IWebElement con_recyclingFeePopUp;

        [FindsBy(How = How.Id, Using = "confirm-submission")]
        public IWebElement btn_Continuebtn;

        [FindsBy(How = How.Id, Using = "mainwrapper")]
        public IWebElement div_mainwrapper;

        [FindsBy(How = How.ClassName, Using = "paypal-button-content")]
        public IWebElement btn_paypalCheckout;

        [FindsBy(How = How.Id, Using = "email")]
        public IWebElement btn_paypalEmail;

        [FindsBy(How = How.Id, Using = "password")]
        public IWebElement btn_paypalPassword;

        [FindsBy(How = How.Id, Using = "btnLogin")]
        public IWebElement btn_paypalLogin;

        [FindsBy(How = How.Id, Using = "confirmButtonTop")]
        public IWebElement btn_paypalContinuePayment;

        //public static object rdbtn_PaymentPage = new { TageName = "input", Id = "PaymentMethod" };
        [FindsBy(How = How.Id, Using = "PaymentMethod")]
        public IWebElement rdbtn_PaymentPage;


        //public static object btn_PlaceOrder = new { Text = "Place Order" };
        [FindsBy(How = How.PartialLinkText, Using = "Place Order")]
        public IWebElement btn_PlaceOrder;

        [FindsBy(How = How.Id, Using = "continue")]
        public IWebElement btn_Continuefinance;

        /// <summary>
        /// Paypal Payment details are entered
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="passWord"></param>
        /// <returns></returns>
        public string payPalPayment(String userName, String passWord)
        {
            try {
                Archive.WaitForElement(By.ClassName("radio-option"));
                IWebElement list_paymentType = div_mainwrapper.FindElement(By.Id("pay-pal"));
                IWebElement ele_paypapRadiobtn = list_paymentType.FindElement(By.ClassName("radio-option"));
                ele_paypapRadiobtn.Click();
                Archive.WaitForPageLoad();
                
                btn_paypalCheckout.Click();
                Archive.WaitForPageLoad();

               // IWebDriver driver = GlobalProperties.driver;
                string currentWindowHandle = Config.driver.CurrentWindowHandle;
                Config.driver.SwitchTo().Window(Config.driver.WindowHandles.Last());

                Config.driver.Manage().Window.Maximize();

                IWebElement iframe_object = Config.driver.FindElement(By.Name("injectedUl"));

                Config.driver.SwitchTo().Frame(iframe_object);
                
                btn_paypalEmail.SendKeys(userName);

                btn_paypalPassword.SendKeys(passWord);

                btn_paypalLogin.Click();
                Thread.Sleep(5000);

                Config.driver.SwitchTo().DefaultContent();
                btn_paypalContinuePayment.Click();
                Thread.Sleep(5000);

                Config.driver.SwitchTo().Window(currentWindowHandle);
                Thread.Sleep(2000);
                result = "Entering Paypal payment details are succesful";
            }
            catch (Exception ex)
            {
                result = "Failure in Entering paypal payment details" + ex.Message + ex.StackTrace;
            }

            return result;


        }

        /// <summary>
        /// Credit Card payment details are entered
        /// </summary>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <param name="cardNumber"></param>
        /// <param name="securityCode"></param>
        /// <param name="expMonth"></param>
        /// <param name="expYear"></param>
        /// <returns></returns>
        public string enterCreditPayments(String firstName, String lastName, String cardNumber, String securityCode, String expMonth, String expYear)
        {
            try
            {
                Archive.WaitForElement(By.Id("CCard_CardHolderFirstName"));
                txt_firstNameOnCard.SendKeys(firstName);
                txt_lastNameOnCard.SendKeys(lastName);
                txt_cardNumber.SendKeys(cardNumber);
                txt_securityCode.SendKeys(securityCode);
                var selectElement = new SelectElement(txt_expMonthOnCard);
                selectElement.SelectByText(expMonth);
                selectElement = new SelectElement(txt_expYearOnCard);
                selectElement.SelectByText(expYear);

                btn_Continuebtn.Click();
                //Assert.IsTrue(CommonFunctions.waitUntilElementDisappear(HomePage.ajax_Loader));
                Archive.WaitForPageLoad();
                result = "Entering Credit card payment details are succesful";
            }
            catch (Exception ex)
            {
                result = "Failure in Entering Credit Card payment details" + ex.Message + ex.StackTrace;
            }
            return result;
        }

        /// <summary>
        /// Ashley card payment details are entered
        /// </summary>
        /// <param name="ashleyCardNumber"></param>
        /// <param name="financingOption"></param>
        /// <returns></returns>
        public string enterAshleyAdvantageCardPayments(string ashleyCardNumber, string financingOption)
        {
            try
            {
                Archive.WaitForElement(By.ClassName("radio-option"));               
                IList<IWebElement> ashleyRadio = con_radioButton[1].GetDescendants();
                int Option_Index = ashleyRadio.IndexOf(ashleyRadio.First(x => x.TagName.Contains("label")));
                ashleyRadio[Option_Index].Click();
                txt_ashleyCardNumber.SendKeys(ashleyCardNumber);

                //IWebElement ele_rdbtn_FinancingOptionsParent = rdbtn_FinancingOptionsParent.Init(PropertyExpression.Contains);
                List<IWebElement> FinancingOptionsList = rdbtn_FinancingOptionsParent.FindElements(By.TagName("label")).ToList();
                // List<IWebElement> FinancingOptionsList = ele_rdbtn_FinancingOptionsParent.GetDescendants();
                Option_Index = FinancingOptionsList.IndexOf(FinancingOptionsList.First(x => x.Text.Contains(financingOption)));
                FinancingOptionsList[Option_Index].Click();


                btn_viewFinancingTerm.Click();
                Archive.WaitForPageLoad();
                Archive.WaitForElement(By.Id("accept-terms-button"));            
                btn_termsOverlayAccept.Click();
                Archive.WaitForPageLoad();
                result = "Entering Ashleyadvantage card payment details are succesful";
            }
            catch (Exception ex)
            {
                result = "Failure in entering Ashleyadvantage card payment details" + ex.Message + ex.StackTrace;
            }

            return result;


        }

    }
}

﻿using AshleyAutomationLibrary;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteCore_Regression_Automation.Pages
{
    public class LoginPage
    {
        public static string result = string.Empty;

        
        public LoginPage()
        {
            PageFactory.InitElements(Config.driver, this);
        }

        [FindsBy(How = How.LinkText, Using = "My Account")]
        public IWebElement ele_myAccount;

        [FindsBy(How = How.Id, Using = "LoginControl_TbxEmailLogin")]
        public IWebElement txt_userName;

        [FindsBy(How = How.Id, Using = "LoginControl_TbxPassword")]
        public IWebElement txt_password;

        [FindsBy(How = How.Id, Using = "LoginControl_SignInButton")]
        public IWebElement btn_signIn;

        [FindsBy(How = How.Id, Using = "scphrightsection_0_TbxCreateFirstName")]
        public IWebElement txt_firstName;

        [FindsBy(How = How.Id, Using = "scphrightsection_0_TbxCreateLastName")]
        public IWebElement txt_lastName;

        [FindsBy(How = How.Id, Using = "scphrightsection_0_TbxCreateEmail")]
        public IWebElement txt_email;

        [FindsBy(How = How.Id, Using = "scphrightsection_0_TbxCreateConfirmEmail")]
        public IWebElement txt_confirmEmail;

        [FindsBy(How = How.Id, Using = "scphrightsection_0_TbxCreatePassword")]
        public IWebElement txt_createPassword;

        [FindsBy(How = How.Id, Using = "scphrightsection_0_TbxCreateConfirmPassword")]
        public IWebElement txt_confirmPassword;

        [FindsBy(How = How.Id, Using = "scphrightsection_0_ChbxUserAgeConfirm")]
        public IWebElement chk_ageConfirm;

        [FindsBy(How = How.Id, Using = "scphrightsection_0_CreateAccountButton")]
        public IWebElement btn_createAccount;

        [FindsBy(How = How.ClassName, Using = "button small")]
        public IWebElement btn_continueGuest;

        [FindsBy(How = How.ClassName, Using = "my-account-wrapper")]
        public IWebElement div_overallWrapper;

        [FindsBy(How = How.Id, Using = "CreateAccount")]
        public IWebElement CreateAccount_Wrapper;

        [FindsBy(How = How.Id, Using = "LogInPanel")]
        public IWebElement OrderStatus_LogInPanel_Wrapper;

        [FindsBy(How = How.PartialLinkText, Using = "Account Sign Up")]
        public IWebElement btn_Account_Sign_Up;

        [FindsBy(How = How.Name, Using = "ConfirmationNumber")]
        public IWebElement txt_confirmationNumber;

        [FindsBy(How = How.Name, Using = "OrderEmail")]
        public IWebElement txt_orderEmail;

        [FindsBy(How = How.Name, Using = "BillingZip")]
        public IWebElement txt_billingZip;

        [FindsBy(How = How.PartialLinkText, Using = "Review Order")]
        public IWebElement btnReviewOrder;

        //mandatory fields for order status

        [FindsBy(How = How.PartialLinkText, Using = "Order Placed:")]
        public IWebElement lblOrderPlaced;

        [FindsBy(How = How.ClassName, Using = "order-date value")]
        public IWebElement lblOrderPlacedValue;

        [FindsBy(How = How.PartialLinkText, Using = "Recipient:")]
        public IWebElement lblRecepient;

        [FindsBy(How = How.ClassName, Using = "order-number value")]
        public IWebElement lblRecepientValue;

        [FindsBy(How = How.PartialLinkText, Using = "Total:")]
        public IWebElement lblTotal;

        [FindsBy(How = How.ClassName, Using = "order-total value")]
        public IWebElement lblTotalValue;

        [FindsBy(How = How.ClassName, Using = "listItemsOrder")]
        public IWebElement lstAddress;

        [FindsBy(How = How.ClassName, Using = "content-Total")]
        public IWebElement lbl_ContentTotal;

        [FindsBy(How = How.LinkText, Using = "Item")]
        public IWebElement header_Item;

        [FindsBy(How = How.LinkText, Using = "Qty")]
        public IWebElement header_Quantity;

        [FindsBy(How = How.LinkText, Using = "QTY")]
        public IWebElement header_QuantityForProfiledUser;

        [FindsBy(How = How.LinkText, Using = "Summary")]
        public IWebElement header_Summary;

        [FindsBy(How = How.LinkText, Using = "Total")]
        public IWebElement header_Total;

        [FindsBy(How = How.ClassName, Using = "ImageWrapper")]
        public IWebElement product_ImageWrapper;

        [FindsBy(How = How.ClassName, Using = "msax-Shipping")]
        public IWebElement productSummaryDetails;

        [FindsBy(How = How.Id, Using = "TbxQuantity")]
        public IWebElement productQuantityInfo;

        [FindsBy(How = How.ClassName, Using = "table-Subtotal")]
        public IWebElement subTotalTable;

        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;

        //public bool verifyOffLineBanner(Object obj, String strDescription)
        //{
        //    bool isVerified = false;
        //    try
        //    {
        //        List<IWebElement> lst_CreateAccount_Wrapper = obj.Init(PropertyExpression.Contains).GetDescendants();
        //        int Index = lst_CreateAccount_Wrapper.IndexOf(lst_CreateAccount_Wrapper.First(x => x.GetAttribute("class").Contains("banner-title")));
        //        isVerified = lst_CreateAccount_Wrapper[Index].Displayed;
        //        //isVerified = WebArchive.IsVisible(lst_CreateAccount_Wrapper[Index]);
        //        TestContext.WriteLog("PASSED:" + strDescription);
        //    }
        //    catch (Exception e)
        //    {
        //        TestContext.WriteLog("FAILED:" + strDescription + e.Message);
        //        Console.Out.WriteLine(e.Message);
        //        isVerified = false;
        //    }
        //    return isVerified;
        //}

        public string signIn(string UserName, string Password)
        {
            try
            {
                txt_userName.SendKeys(UserName);

                txt_password.SendKeys(Password);

                btn_signIn.MouseOver();
                btn_signIn.Click();
                Archive.WaitForPageLoad();

                result = "SignIn in has been performed succesfully";
            }
            catch (Exception ex)
            {
                result = "Failure in Sign In Functionality" + ex.Message + ex.StackTrace;
            }

            return result;

        }

        public bool enterDetailsInUnregisteredCustomerOrderStatus(String confirmationNumber, String emailID, String zipCode, String logDescription)
        {
            bool isEntered = false;

            try
            {
                Archive.WaitForElement(By.Name("ConfirmationNumber"));
                //CommonFunction.enterValueInTextbox(txt_confirmationNumber, confirmationNumber);
                txt_confirmationNumber.SendKeys(confirmationNumber);

                //CommonFunction.enterValueInTextbox(txt_orderEmail, emailID);
                txt_orderEmail.SendKeys(emailID);
                //isEntered = CommonFunction.enterValueInTextbox(txt_billingZip, zipCode);

            }
            catch (Exception e)
            {
                System.Console.Out.WriteLine(e.Message);
                isEntered = false;
                TestContext.WriteLog("FAILED:" + logDescription);
            }

            return isEntered;
        }

        public string createAccount(string FirstName, string LastName, string Email, string Password)
        {
            try
            {
                txt_firstName.SendKeys(FirstName);

                txt_lastName.SendKeys(LastName);

                txt_email.SendKeys(Email);

                txt_confirmEmail.SendKeys(Email);

                txt_createPassword.SendKeys(Password);

                txt_confirmPassword.SendKeys(Password);

                chk_ageConfirm.Click();

                btn_createAccount.Click();
                Archive.WaitForPageLoad();
                result = "Creating Account has been performed succesfully";
            }
            catch (Exception ex)
            {
                result = "Failure in creating an account" + ex.Message + ex.StackTrace;
            }

            return result;


        }
    }
}

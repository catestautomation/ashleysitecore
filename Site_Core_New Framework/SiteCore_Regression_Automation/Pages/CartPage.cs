﻿using AshleyAutomationLibrary;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using SiteCore_Regression_Automation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteCore_Regression_Automation.Pages
{
    public  class CartPage
    {

        private  string strReplaceContent;
        public string result = string.Empty;

        public CartPage()
        {
            PageFactory.InitElements(Config.driver, this);
        }

        [FindsBy(How = How.ClassName, Using = "view-cart-content")]
        public  IWebElement con_cartContent;

        [FindsBy(How = How.Id, Using = "UpdateZipcodeBtn")]
        public  IWebElement btn_update;

        [FindsBy(How = How.Id, Using = "zipCodeTextbox")]
        public  IWebElement txt_zipCode;

        [FindsBy(How = How.Id, Using = "toPopup1")]
        public  IWebElement con_promoCodePopUp;

        [FindsBy(How = How.ClassName, Using = "msax-Promocodeitem")]
        public  IWebElement mge_promoCodeValid;

        [FindsBy(How = How.Id, Using = "lblError1")]
        public  IWebElement lbl_errorMge;

        [FindsBy(How = How.Id, Using = "stateRecyclingFee")]
        public  IWebElement con_recyclingFeePopUp;

        [FindsBy(How = How.ClassName, Using = "msax-ContinueShoppingButton")]
        public  IWebElement btn_continueShoppingAtCart;

        [FindsBy(How = How.ClassName, Using = "close")]
        public  IWebElement btn_closeLightBox;

        [FindsBy(How = How.Id, Using = "applyPromotionCodeID")]
        public  IWebElement btn_enterPromoCodeLightBox;

        [FindsBy(How = How.Id, Using = "rsaIndicatorView")]
        public  IWebElement con_rsaIndicatorOption;

        [FindsBy(How = How.Id, Using = "showRsaIndicatorBlock")]
        public  IWebElement lnk_editRsaIndicatorBlock;

        [FindsBy(How = How.Id, Using = "addToWishList")]
        public  IWebElement con_addToWishListPopUp;

        [FindsBy(How = How.Id, Using = "newWishListName")]
        public  IWebElement txt_newWishListName;

        [FindsBy(How = How.Id, Using = "createWhishListSuccess")]
        public  IWebElement con_createWishListSuccessPopUp;

        [FindsBy(How = How.Id, Using = "WishListContinueShopping")]
        public  IWebElement btn_continueShoppingButton;

        [FindsByAll]
        [FindsBy(How = How.PartialLinkText, Using = "Checkout",Priority = 0)]
        [FindsBy(How = How.TagName, Using = "button", Priority = 1)]
        public  IWebElement btn_NewCheckOutCartPage;


        [FindsBy(How = How.XPath, Using = ("/descendant::span[contains(.,'Proceed to Checkout')]"))]
        public  IWebElement btn_checkOutCartPage;

        [FindsBy(How = How.ClassName, Using = "msax-PromotionCodeText")]
        public  IWebElement txt_promoCodeField;

        [FindsBy(How = How.ClassName, Using = "msax-PromotionCodeText")]
        public  IWebElement btn_promoCodeButton;

        [FindsBy(How = How.ClassName, Using = "search-box__input")]
        public  IWebElement txt_cartPageSearchInputField;
        
        [FindsBy(How = How.ClassName, Using = "search-box__icon")]
        public  IWebElement btn_CartPageSearchInputbutton;

        //public  object btn_CartPageSearchInputbutton = new { Id = "searchIcon" };//Ashcomm5//Ashcomm6
        //public  object lbl_shippingCharge = new {  Class = "msax-EstHomeDelivery" };
        [FindsBy(How = How.ClassName, Using = "msax-EstHomeDelivery")]
        public  IWebElement lbl_shippingCharge;


        [FindsBy(How = How.ClassName, Using = "msax-ContinueShoppingButton")]
        public  IWebElement lnk_continueShoppingAtCartPage;
        
        [FindsByAll]
        [FindsBy(How = How.ClassName, Using = "product-summary__list", Priority = 0)]
        [FindsBy(How = How.TagName, Using = "ul", Priority = 1)]
        public  IWebElement productSummaryListAtCartPage;

 
        [FindsByAll]
        [FindsBy(How = How.ClassName, Using = "msax-ProductId", Priority = 0)]
        public IList<IWebElement> lbl_skuNumberAtCartPage;

        [FindsByAll]
        [FindsBy(How = How.ClassName, Using = "msax-ProductName", Priority = 0)]
        public IList<IWebElement> lbl_productNameAtCartPage;

        [FindsByAll]
        [FindsBy(How = How.ClassName, Using = "msax-ImageWrapper", Priority = 0)]
        public IList<IWebElement> lbl_productImageAtCartPage;

        [FindsByAll]
        [FindsBy(How = How.XPath, Using = "//*[.='Qty:']")]
        public IList<IWebElement> lbl_productQuantityAtCartPage;

        [FindsByAll]
        [FindsBy(How = How.ClassName, Using = "msax-SoldBy", Priority = 0)]
        public IList<IWebElement> lbl_fulfilledByAtCartPage;


        [FindsBy(How = How.PartialLinkText, Using = "CHECKOUT")]
        public  IWebElement btn_CHECKOUT;

        [FindsBy(How = How.PartialLinkText, Using = "Checkout")]
        public  IWebElement btn_Checkout;

        [FindsBy(How = How.PartialLinkText, Using = "CHECKOUT")]
        public  IWebElement btn_CONTINUESHOPPING;

        [FindsBy(How = How.PartialLinkText, Using = "Continue Shopping")]
        public  IWebElement btn_ContinueShopping;



    }
}

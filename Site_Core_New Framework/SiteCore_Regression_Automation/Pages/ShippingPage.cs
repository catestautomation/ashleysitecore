﻿using OpenQA.Selenium;
using AshleyAutomationLibrary;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Support.UI;
using System.Threading;

namespace SiteCore_Regression_Automation.Pages
{
    public class ShippingPage
    {
        public static string result = string.Empty;

        public ShippingPage()
        {
            PageFactory.InitElements(Config.driver, this);
        }

        [FindsBy(How = How.ClassName, Using = "msax-UseAddressButton")]
        public IWebElement ele_btn_useThisAddress;

        [FindsBy(How = How.Id, Using = "stateRecyclingFee")]
        public IWebElement ele_con_recyclingFeePopUp;

        [FindsBy(How = How.Id, Using = "OrderAddressFirstName")]
        public IWebElement ele_txt_firstName;

        [FindsBy(How = How.Id, Using = "OrderAddressLastName")]
        public IWebElement ele_txt_lastName;

        [FindsBy(How = How.Id, Using = "OrderAddressStreet1")]
        public IWebElement ele_txt_addressLine1;

        [FindsBy(How = How.Id, Using = "OrderAddressCity")]
        public IWebElement ele_txt_city;

        [FindsBy(How = How.Id, Using = "OrderAddressState")]
        public IWebElement ele_dd_state;

        [FindsBy(How = How.Id, Using = "OrderAddressZipCode")]
        public IWebElement ele_txt_zipCode;

        [FindsBy(How = How.Id, Using = "OrderPhoneNumber1")]
        public IWebElement ele_txt_phone;

        [FindsBy(How = How.Id, Using = "ShippingEmail")]
        public IWebElement ele_txt_email;

        [FindsBy(How = How.Id, Using = "ShippingConfirmEmail")]
        public IWebElement ele_Txt_confirmEmail;

        [FindsBy(How = How.XPath, Using = "/descendant::button[@class='msax-Next'][1]")]
        public IWebElement ele_btn_continue;

        [FindsBy(How = How.Id, Using = "msax-CheckOutControl")]
        public IWebElement ele_div_overallContainer;

        public string addShippingAddress(String firstName, String lastName, String address, String city, String stateCode,
             String phoneNumber, String zipCode, String email)
        {
            try
            {

                ele_txt_firstName.SendKeys(firstName);

                ele_txt_lastName.SendKeys(lastName);

                ele_txt_addressLine1.SendKeys(address);

                ele_txt_city.SendKeys(city);

                var selectElement = new SelectElement(ele_dd_state);
                selectElement.SelectByText(stateCode);

                ele_txt_phone.SendKeys(phoneNumber);

                ele_txt_zipCode.Clear();
                ele_txt_zipCode.SendKeys(zipCode);

                ele_txt_email.SendKeys(email);

                ele_Txt_confirmEmail.SendKeys(email);

                ele_btn_continue.Click();

                Archive.WaitForPageLoad();
                // CommonFunction.waitForPageLoad();

                Thread.Sleep(10000);
                result = "Adding shipping details are performed succesfully";
            }
            catch (Exception ex)
            {
                result = "Failure in adding shipping details" + ex.Message + ex.StackTrace;
            }

            return result;
        }
    }
}

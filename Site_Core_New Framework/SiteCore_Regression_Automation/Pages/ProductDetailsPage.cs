﻿using AshleyAutomationLibrary;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SiteCore_Regression_Automation.Pages
{
    public class ProductDetailsPage
    {
        public ProductDetailsPage()
        {
            PageFactory.InitElements(Config.driver, this);
        }

        public static string result = string.Empty;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'Shopbuttons')]")]
        public IWebElement btn_shopButtons;

        //public static object btn_quickViewshopButtons = new { Class = "product-quick-view-overlay__actions" };

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'product-quick-view-overlay__actions')]")]
        public IWebElement btn_quickViewshopButtons;

        [FindsBy(How = How.ClassName, Using = "product-grid__showing-items")]
        public IWebElement footer_ProductCount;

        [FindsBy(How = How.Id, Using = "AddToCartProductDetail")]
        public IWebElement btn_addToCart;

        [FindsBy(How = How.Id, Using = "addToCartQuickView")]
        public IWebElement btn_addToCartAtQuickView;

        [FindsBy(How = How.ClassName, Using = "afhs-content")]
        public IWebElement con_product;

        [FindsBy(How = How.ClassName, Using = "productQuantity")]
        public IWebElement txt_quantity;

        [FindsBy(How = How.Id, Using = "carousel-3")]
        public IWebElement img_productDetailedImage;

        [FindsBy(How = How.ClassName, Using = "SocialIconsList clearfix")]
        public IWebElement icn_socialMedia;

        [FindsBy(How = How.Id, Using = "scphbody_0_prodFlagDiv")]
        public IWebElement flg_productFlag;

        [FindsBy(How = How.Id, Using = "TbxAvailabilityZipCode")]
        public IWebElement txt_zipCode;

        [FindsBy(How = How.Id, Using = "AvailabilitySearchButton")]
        public IWebElement btn_zipCodeSearch;

        [FindsBy(How = How.ClassName, Using = "zip-code-overlay__input")]
        public IWebElement txt_quickViewZipCode;

        [FindsBy(How = How.ClassName, Using = "afhs-logo-img")]
        public IWebElement img_AshleyLogo;

        [FindsBy(How = How.XPath, Using = "//button[contains(@class,'zip-code-overlay__button')]")]
        public IWebElement btn_quickViewZipCodeSearch;

        [FindsBy(How = How.Id, Using = "TbxSearch")]
        public IWebElement txt_searchProduct;

        [FindsBy(How = How.Id, Using = "ctl09_searchIcon2")]
        public IWebElement btn_search;

        [FindsBy(How = How.Id, Using = "addToCart")]
        public IWebElement con_productLightBox;

        [FindsBy(How = How.LinkText, Using = "Checkout")]
        public IWebElement btn_quickViewcheckout;

        [FindsBy(How = How.ClassName, Using = "wish-list-overlay__select")]
        public IWebElement slct_WishList;

        [FindsBy(How = How.Id, Using = "DdlWishLists")]
        public IWebElement slct_PDPWishList;

        [FindsBy(How = How.ClassName, Using = "wish - list - overlay__option")]
        public IWebElement drpdwn_WishLists;

        [FindsBy(How = How.Id, Using = "newWishListName")]
        public IWebElement txt_createWishList;

        [FindsBy(How = How.LinkText, Using = "CREATE")]
        public IWebElement btn_createButton;

        [FindsBy(How = How.LinkText, Using = "CONTINUE SHOPPING")]
        public IWebElement btn_wishListContinueShopping;

        [FindsBy(How = How.LinkText, Using = "VIEW WISH LIST")]
        public IWebElement btn_wishListViewWishList;

        [FindsBy(How = How.XPath, Using = "//button[.='Continue Shopping']")]
        public IWebElement btn_continueShopping;

        [FindsBy(How = How.LinkText, Using = "Checkout")]
        public IWebElement btn_checkOut;

        [FindsBy(How = How.LinkText, Using = "CONTINUE SHOPPING")]
        public IWebElement btn_PDPContinueShopping;

        [FindsBy(How = How.XPath, Using = "//a[.='CHECKOUT']")]
        public IWebElement btn_PDPCheckOut;

        [FindsBy(How = How.ClassName, Using = "main-content")]
        public IWebElement div_mainContent;

        [FindsBy(How = How.ClassName, Using = "contentCenter")]
        public IWebElement div_ContentCentre;

        [FindsBy(How = How.ClassName, Using = "zoomContainer")]
        public IWebElement div_imageContainer;

        [FindsBy(How = How.ClassName, Using = "drop1")]
        public IWebElement div_SocialMediaContainer;

        [FindsBy(How = How.Id, Using = "scphbody_0_Quantity")]
        public IWebElement txt_enterQuantity;

        [FindsBy(How = How.ClassName, Using = "medium-12 ProductDescription")]
        public IWebElement lbl_productDescription;

        [FindsBy(How = How.ClassName, Using = "SocialIconsList clearfix")]
        public IWebElement hyp_socialMediaIcons;

        [FindsBy(How = How.ClassName, Using = "options columns medium-9")]
        public IWebElement lbl_colorSwatch;

        [FindsBy(How = How.Id, Using = "scphbody_0_Quantity")]
        public IWebElement txt_productQuantity;

        [FindsBy(How = How.ClassName, Using = "wish-list-overlay__image")]
        public IWebElement img_wishListOverlay;

        [FindsBy(How = How.ClassName, Using = "wish-list-overlay__title")]
        public IWebElement title_wishListOverlay;

        [FindsBy(How = How.ClassName, Using = "popupimg")]
        public IWebElement img_checkOutOverlayPopup;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'popupinfo')]")]
        public IWebElement title_checkOutOverlayPopup;

        [FindsBy(How = How.ClassName, Using = "title-item")]
        public IWebElement title_wishListItemName;

        [FindsBy(How = How.ClassName, Using = "tile-icon")]
        public IWebElement img_wishListItemImage;

        [FindsBy(How = How.ClassName, Using = "Price")]
        public IWebElement lbl_wishListItemPrice;

        [FindsBy(How = How.ClassName, Using = "colorItem")]
        public IWebElement lbl_wishListItemQuantity;

        //public static Object lbl_wishListItemId = new { Class = "colorItem", Text = "#" };
        [FindsBy(How = How.ClassName, Using = "colorItem")]
        public IWebElement lbl_wishListItemId;

        [FindsBy(How = How.ClassName, Using = "detailsItem")]
        public IWebElement lbl_wishListDetailsItem;

        [FindsBy(How = How.ClassName, Using = "image-gallery-swipe")]
        public IWebElement img_quickViewOverlayProductImage;

        [FindsBy(How = How.ClassName, Using = "product-quick-view-overlay__title")]
        public IWebElement lbl_quickViewOverlayProductName;

        [FindsBy(How = How.ClassName, Using = "product-quick-view-overlay__summary")]
        public IWebElement lbl_quickViewOverlayProductDescription;

        [FindsBy(How = How.ClassName, Using = "product-price__price product-price__price--regular")]
        public IWebElement lbl_quickViewOverlayProductPrice;

        [FindsBy(How = How.ClassName, Using = "product-quick-view-overlay__quantity-label")]
        public IWebElement lbl_quickViewOverlayProductQuantity;

        [FindsBy(How = How.ClassName, Using = "product-quick-view-overlay__link")]
        public IWebElement lbl_quickViewOverlayViewProductDetails;

        [FindsBy(How = How.ClassName, Using = "cart-response-overlay__image")]
        public IWebElement img_checkOutOverlayProductImage;

        [FindsBy(How = How.ClassName, Using = "cart-response-overlay__title cart-response-overlay__title--product")]
        public IWebElement lbl_checkOutOverlayProductName;

        [FindsByAll]
        [FindsBy(How = How.ClassName, Using = "product-price")]
        public IList<IWebElement> lbl_checkOutOverlayProductPrice;

        [FindsBy(How = How.ClassName, Using = "cart-response-overlay__item")]
        public IWebElement lbl_checkOutOverlayProductQuantity;

        [FindsBy(How = How.ClassName, Using = "cart-response-overlay__item cart-response-overlay__item--shipping-message")]
        public IWebElement lbl_checkOutOverlayShippingDays;

        [FindsBy(How = How.ClassName, Using = "priceColumn")]
        public IWebElement lbl_colorSwatchProductPrice;

        [FindsBy(How = How.ClassName, Using = "priceColumn")]
        public IWebElement lbl_colorSwatchProductName;

        [FindsBy(How = How.ClassName, Using = "productFluffDesc")]
        public IWebElement lbl_colorSwatchProductDescription;

        [FindsBy(How = How.ClassName, Using = "zoomContainer")]
        public IWebElement img_colorSwatchProductDetailImage;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'product-quick-view-overlay__actions')]")]
        public IWebElement btn_QuickViewOverlayActions;




        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;


        /// <summary>
        /// Verify the absence of buttons in ProductDetailsPage
        /// </summary>
        /// <param name="QuickView"></param>
        /// <param name="strText"></param>
        /// <param name="StrLogDescription"></param>
        /// <returns></returns>
        public bool verifyAbsenceOfButtonInProductDetailsPage(bool QuickView, String strText, String StrLogDescription)
        {
            bool isNotVerified = false;
            try
            {
                if (QuickView == true)
                {
                    List<IWebElement> lstButtons = btn_QuickViewOverlayActions.GetDescendants();
                    for (int i = 0; i < lstButtons.Count; i++)
                    {
                        if (!lstButtons[i].Text.Contains(strText))
                        {
                            isNotVerified = true;
                            TestContext.WriteLog("PASSED:" + StrLogDescription);
                            //Testcontext.WriteLog("PASSED:" + StrLogDescription);
                        }
                        else
                        {
                            isNotVerified = false;
                            TestContext.WriteLog("FAILED:" + StrLogDescription);
                            //WebArchive.WriteLog("FAILED:" + StrLogDescription);
                            break;
                        }
                    }

                }
                else
                {
                    List<IWebElement> lstButtons = btn_shopButtons.GetDescendants();
                    for (int i = 0; i < lstButtons.Count; i++)
                    {
                        if (!lstButtons[i].Text.Contains(strText))
                        {
                            isNotVerified = true;
                            TestContext.WriteLog("PASSED:" + StrLogDescription);
                        }
                        else
                        {
                            isNotVerified = false;
                            TestContext.WriteLog("FAILED:" + StrLogDescription);
                            break;
                        }
                    }
                }

            }
            catch (Exception e)
            {
                isNotVerified = false;
                TestContext.WriteLog("FAILED:" + StrLogDescription);
                Console.Out.WriteLine(e.Message);
            }
            return isNotVerified;
        }

        /// <summary>
        /// Adding a product to the cart through PDP Page or Quick view Pop up
        /// </summary>
        /// <param name="zipCode"></param>
        /// <param name="quantity"></param>
        /// <param name="IsQuickView"></param>
        /// <param name="noZipCode"></param>
        /// <returns></returns>
        public string addToCart(string zipCode = "", string quantity = "1", bool IsQuickView = false, bool noZipCode = false)
        {
            try
            {
                if (IsQuickView == false)
                {
                    Archive.WaitForElement(By.XPath("//div[contains(@class,'Shopbuttons')]"));
                    //IWebElement ele_btn_shopButtons = btn_shopButtons.Init(PropertyExpression.StartsWith);
                    List<IWebElement> btn_lists = btn_shopButtons.FindElements(By.TagName("a")).ToList();
                    for (int i = 0; i < btn_lists.Count; i++)
                    {
                        if (btn_lists[i].Text.ToLower().Contains("Check Price".ToLower()))
                        {
                            //btn_lists[i].ClickControl();
                            btn_lists[i].Click();
                            Archive.WaitForPageLoad();
                            txt_zipCode.SendKeys(zipCode);
                            btn_zipCodeSearch.Click();
                            Archive.WaitForPageLoad();
                            txt_enterQuantity.Clear();
                            txt_enterQuantity.SendKeys(quantity);
                            //ele_btn_addToCart.ClickControl();
                            btn_addToCart.Click();
                            Archive.WaitForPageLoad();
                            break;
                        }
                        else if (btn_lists[i].Text.ToLower().Contains("Add to Cart".ToLower()))
                        {
                            if (noZipCode == true)
                            {
                                txt_enterQuantity.Clear();
                                txt_enterQuantity.SendKeys(quantity);
                                btn_lists[i].Click();
                                Archive.WaitForPageLoad();
                                break;
                            }
                            else
                            {
                                txt_enterQuantity.Clear();
                                txt_enterQuantity.SendKeys(quantity);
                                //btn_lists[i].ClickControl();
                                btn_lists[i].Click();
                                Archive.WaitForPageLoad();

                                txt_zipCode.SendKeys(zipCode);

                                //ele_btn_zipCodeSearch.ClickControl();
                                btn_zipCodeSearch.Click();
                                Archive.WaitForPageLoad();
                                break;
                            }

                        }
                        /*for loop breaks here*/
                    }
                }
                else
                {
                    Archive.WaitForElement(By.XPath("//div[contains(@class,'product-quick-view-overlay__actions')]"));
                    List<IWebElement> btn_quickViewLists = btn_quickViewshopButtons.FindElements(By.TagName("span")).ToList();

                    for (int i = 0; i < btn_quickViewLists.Count; i++)
                    {
                        if (btn_quickViewLists.Count != 0)
                        {
                            if (btn_quickViewLists[i].Text.ToLower().Contains("CHECK PRICE".ToLower()))
                            {
                                Thread.Sleep(3000);
                                btn_quickViewLists[i].Click();
                                Archive.WaitForElement(By.ClassName("zip-code-overlay__input"));
                                txt_quickViewZipCode.SendKeys(zipCode);
                                //ele_btn_quickViewZipCodeSearch.ClickControl();
                                btn_quickViewZipCodeSearch.Click();
                                Archive.WaitForPageLoad();
                                break;
                            }

                        }
                        /*for loop breaks here*/
                    }
                    Archive.WaitForElement(By.XPath("//div[contains(@class,'product-quick-view-overlay__actions')]"));
                    List<IWebElement> btn_quickViewAddToCartLists = btn_quickViewshopButtons.FindElements(By.TagName("button")).ToList();
                    btn_quickViewAddToCartLists[0].Click();
                    Archive.WaitForPageLoad();
                    if (noZipCode == true)
                    {

                        btn_quickViewshopButtons.SendKeys(zipCode);
                        btn_quickViewZipCodeSearch.Click();
                        btn_quickViewZipCodeSearch.Click();
                        Archive.WaitForPageLoad();
                    }
                }
                result = "Adding a Product to the cart is succesful - QuickViewPopup -" + IsQuickView;
            }
            catch (Exception ex)
            {
                result = "Failure in adding a Product to the cart - QuickViewPopup -" + IsQuickView + ex.Message + ex.StackTrace;
            }

            return result;

        }

        /// <summary>
        /// Adding a product to the Wish listfrom PDP Page or Quick View Pop up
        /// </summary>
        /// <param name="IsQuickView"></param>
        /// <returns></returns>
        public string addToList(bool IsQuickView = false)
        {
            try
            {
                if (IsQuickView == false)
                {
                    List<IWebElement> btn_lists = btn_shopButtons.FindElements(By.TagName("a")).ToList();
                    if (btn_lists[0].Text.Contains("Check Price"))
                    {
                        btn_lists[1].Click();
                        Archive.WaitForPageLoad();
                        txt_zipCode.SendKeys("60610");
                        btn_zipCodeSearch.Click();
                        Archive.WaitForPageLoad();
                    }
                    else if (btn_lists[3].Text.Contains("Save for Later"))
                    {
                        btn_lists[3].Click();
                        Archive.WaitForPageLoad();
                    }
                }
                else
                {
                    List<IWebElement> btn_quickViewLists = btn_quickViewshopButtons.FindElements(By.TagName("span")).ToList();
                    if (btn_quickViewLists.Count != 0)
                    {
                        if (btn_quickViewLists[0].Text.Contains("CHECK PRICE"))
                        {
                            btn_quickViewLists[0].Click();
                            Archive.WaitForPageLoad();
                            txt_quickViewZipCode.SendKeys("60610");
                            btn_quickViewZipCodeSearch.Click();
                            Archive.WaitForPageLoad();
                        }
                    }
                    List<IWebElement> btn_quickViewAddToWishLists = btn_quickViewshopButtons.FindElements(By.TagName("button")).ToList();
                    btn_quickViewAddToWishLists[1].Click();
                    Archive.WaitForPageLoad();
                }
                Archive.WaitForElement(By.ClassName("popupimg"));
                result = "Adding a Product to the List is succesful - QuickViewPopup -" + IsQuickView;
        }
            catch (Exception ex)
            {
                result = "Failure in adding a Product to the List - QuickViewPopup -" + IsQuickView + ex.Message + ex.StackTrace;
            }
            return result;
        }
    }
}

﻿using AshleyAutomationLibrary;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteCore_Regression_Automation.Pages
{
    public class ConfirmOrderPage
    {
        public ConfirmOrderPage()
        {
            PageFactory.InitElements(Config.driver, this);
        }

        //public static object img_ashleyLogo = new { Class = "afhs-cart-logo" };
        [FindsBy(How = How.ClassName, Using = "afhs-cart-logo")]
        public  IWebElement img_ashleyLogo;

        //public static object elm_shippingAddress = new { Class = "msax-ShippingAddressData" };
        [FindsBy(How = How.ClassName, Using = "msax-ShippingAddressData")]
        public IWebElement elm_shippingAddress;

        //public static object elm_billingAddress = new { Class = "msax-BillingAddressData" };
        [FindsBy(How = How.ClassName, Using = "msax-BillingAddressData")]
        public  IWebElement elm_billingAddress;

        //public static object elm_shippingMethod = new { Class = "msax-ShippingMethod" };
        [FindsBy(How = How.ClassName, Using = "msax-ShippingMethod")]
        public  IWebElement elm_shippingMethod;

        
        [FindsBy(How = How.ClassName, Using = "msax-OrderSummary")]
        public IWebElement elm_orderSummary;

      
        [FindsBy(How = How.ClassName, Using = "msax-ProductName")]
        public IWebElement elm_productName;

        [FindsBy(How = How.ClassName, Using = "msax-QuantityContent")]
        public IWebElement elm_quantity;

        [FindsBy(How = How.ClassName, Using = "msax-LineTotal")]
        public IWebElement elm_total;
 
        [FindsBy(How = How.Id, Using = "scphbody_0_OrderDetailEdit")]
        public IWebElement lnk_editAddress;

        [FindsBy(How = How.Id, Using = "LblProductAdded")]
        public IWebElement elm_labelProduct;

        [FindsBy(How = How.Id, Using = "WishListContinueShopping")]
        public IWebElement btn_continueShoppingButton;

        [FindsBy(How = How.Id, Using = "scphbody_0_termsSubmitWrapper")]
        public IWebElement btn_termsOfSubmitOrderButton;

        [FindsBy(How = How.Id, Using = "termsOfUseLightbox")]
        public IWebElement con_termsOfUseLightBox;

        [FindsBy(How = How.Id, Using = "termsAndConditionsLightbox")]
        public static IWebElement con_termsAndConditionsLightBox;

        [FindsBy(How = How.Id, Using = "privacyPolicyLightbox")]
        public IWebElement con_privacyPolicyLightbox;

        [FindsBy(How = How.Id, Using = "stateRecyclingFee")]
        public IWebElement con_recyclingFeePopUp;

        [FindsBy(How = How.Id, Using = "rsaIndicatorBlock")]
        public IWebElement con_rsaIndicatorBlock;

        [FindsBy(How = How.Id, Using = "showRsaIndicatorBlock")]
        public IWebElement con_editRsaIndicatorBlock;

        [FindsBy(How = How.Id, Using = "navigateAwayModal")]
        public IWebElement con_leavePopUp;

        [FindsByAll]
        [FindsBy(How = How.XPath, Using = "//span[contains(.,'(Edit)')]")]
        public IList<IWebElement> lnk_Address;

        [FindsBy(How = How.Id, Using = "toPopup1")]
        public  IWebElement con_promocodePopUp;

        [FindsBy(How = How.ClassName, Using = "close")]
        public IWebElement btn_closeLightBox;

        [FindsBy(How = How.XPath, Using = "//button[.='Submit order']")]
        public IWebElement btn_submitOrder;

        [FindsBy(How = How.ClassName, Using = "msax-PayPalLogo")]
        public IWebElement img_paypalLogo;

        [FindsBy(How = How.PartialLinkText, Using = "Place Order")]
        public IWebElement btn_PlaceOrder;

        [FindsByAll]
        [FindsBy(How = How.Id, Using = "txtbxQuantityIncrementalID")]
        public IList<IWebElement> txt_quantity;

        [FindsByAll]
        [FindsBy(How = How.ClassName, Using = "msax-ProductName")]
        public IList<IWebElement> lnk_productName;

        [FindsByAll]
        [FindsBy(How = How.ClassName, Using = "msax-ProductId")]
        public  IList<IWebElement> elm_productId;
    }
}

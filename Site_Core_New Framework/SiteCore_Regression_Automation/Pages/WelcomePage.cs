﻿using AshleyAutomationLibrary;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SiteCore_Regression_Automation.Pages
{
    public class WelcomePage
    {
        public WelcomePage()
        {
            PageFactory.InitElements(Config.driver, this);
        }
        public static string result = string.Empty;

        //public static object txt_headers = new { TagName = "h1", Text = "My Account" };
        [FindsBy(How = How.PartialLinkText, Using = "My Account")]
        public IWebElement txt_headers;

        [FindsBy(How = How.XPath, Using = ("//ul[@class='tabs vertical large-3 column hide-for-mobile']"))]
        public IWebElement lnk_listHeaders;

        //IWebElement lnk_listHeaders = Config.driver.FindElement(By.XPath("//ul[@class='tabs vertical large-3 column hide-for-mo*/bile']"));

        //public static object lnk_accountInfo = new { TagName = "a", Text = "Account Info" };
        [FindsBy(How = How.PartialLinkText, Using = "Account Info")]
        public IWebElement lnk_accountInfo;

        //public static object lnk_orderHistory = new { TagName = "a", Text = "Order History" };
        [FindsBy(How = How.PartialLinkText, Using = "Order History")]
        public IWebElement lnk_orderHistory;

        //public static object lnk_addressBook = new { TagName = "a", Text = "Address Book" };
        [FindsBy(How = How.PartialLinkText, Using = "Address Book")]
        public IWebElement lnk_addressBook;

        //public static object lnk_wishLists = new { TagName = "a", Text = "Wish Lists" };
        [FindsBy(How = How.PartialLinkText, Using = "Wish Lists")]
        public IWebElement lnk_wishLists;

        [FindsBy(How = How.Id, Using = "AccountInfoGeneral")]
        public IWebElement txt_accountInfoCollection;

        [FindsBy(How = How.Id, Using = "TbxFirstName")]
        public IWebElement txt_accountInfoNewFirstName;

        [FindsBy(How = How.Id, Using = "TbxLastName")]
        public IWebElement txt_accountInfoNewLastName;

        [FindsBy(How = How.Id, Using = "TbxEmail")]
        public IWebElement txt_accountInfoNewEmail;

        public static object btn_accountInfoNewSave = new { Id = "BtnSaveName" };
        [FindsBy(How = How.Id, Using = "CCard_CNber")]
        public IWebElement txt_cardNumber;

        [FindsBy(How = How.Id, Using = "TbxCurrentPassword")]
        public IWebElement txt_accountInfoEmailPassword;

        [FindsBy(How = How.Id, Using = "TbxConfirmEmail")]
        public IWebElement txt_accountInfoConfirmNewEmail;

        [FindsBy(How = How.Id, Using = "TbxCurrentPassword")]
        public IWebElement txt_accountInfoCurrentPassword;

        [FindsBy(How = How.Id, Using = "TbxNewPassword")]
        public IWebElement txt_accountInfoNewPassword;

        [FindsBy(How = How.Id, Using = "TbxConfirmNewPassword")]
        public IWebElement txt_accountInfoConfirmNewPassword;

        [FindsBy(How = How.Id, Using = "SaveNewPassword")]
        public IWebElement btn_accountInfoPasswordSave;

        [FindsBy(How = How.Id, Using = "SaveEmail")]
        public IWebElement btn_accountInfoEmailSave;

        [FindsBy(How = How.Id, Using = "orders")]
        public IWebElement col_recentOrdersCollection;

        [FindsBy(How = How.ClassName, Using = "confirmationOrder")]
        public IWebElement con_confirmationOrder;

        [FindsBy(How = How.Id, Using = "addNewAddressButton")]
        public IWebElement btn_addAddress;

        [FindsBy(How = How.Id, Using = "chooseSuggestedAddress")]
        public IWebElement btn_useThisAddress;

        [FindsBy(How = How.Id, Using = "TbxEditAddressFirstName")]
        public IWebElement txt_firstName;

        [FindsBy(How = How.Id, Using = "TbxEditAddressLastName")]
        public IWebElement txt_lastName;

        [FindsBy(How = How.Id, Using = "TbxEditAddress")]
        public IWebElement txt_address;

        [FindsBy(How = How.Id, Using = "TbxEditAddressCity")]
        public IWebElement txt_city;

        [FindsBy(How = How.Id, Using = "DdlStates")]
        public IWebElement dd_state;

        [FindsBy(How = How.Id, Using = "TbxEditAddressZip")]
        public IWebElement txt_zipCode;

        [FindsBy(How = How.Id, Using = "TbxEditShippingPrimaryPhone")]
        public IWebElement txt_phoneNumber;

        [FindsBy(How = How.Id, Using = "TbxEditShippingEmail")]
        public IWebElement txt_email;

        [FindsBy(How = How.Id, Using = "TbxEditShippingConfirmEmail")]
        public IWebElement txt_confirmEmail;

        [FindsBy(How = How.PartialLinkText, Using = "(Add Billing Address)")]
        public IWebElement lnk_addBillingAddress;

        [FindsBy(How = How.Id, Using = "TbxEditBillingPrimaryPhone")]
        public IWebElement txt_billingPhoneNumber;

        [FindsBy(How = How.Id, Using = "TbxEditBillingEmail")]
        public IWebElement txt_billingEmail;

        [FindsBy(How = How.Id, Using = "TbxEditBillingConfirmEmail")]
        public IWebElement txt_billingConfirmEmail;

        [FindsBy(How = How.Id, Using = "rowShippingAddress")]
        public IWebElement lbl_enteredShippingAddress;

        [FindsBy(How = How.Id, Using = "contentAllItemsWhislist")]
        public IWebElement con_wishListingContainer;

        [FindsBy(How = How.ClassName, Using = "whishListItemLinkBox")]
        public IWebElement con_wishListItem;

        [FindsBy(How = How.Id, Using = "tabs-content vertical")]
        public IWebElement con_manageAccountContainer;

        [FindsBy(How = How.Id, Using = "shareWishlistTo")]
        public IWebElement txt_shareWishlistTo;

        [FindsBy(How = How.Id, Using = "shareWishlistMessage")]
        public IWebElement txt_shareWishlistMessage;

        [FindsBy(How = How.Id, Using = "shareWishListButton")]
        public IWebElement btn_shareWishListButton;

        [FindsBy(How = How.Id, Using = "TxtEditWishlistName")]
        public IWebElement txt_editWishlistName;

        //public static object btn_editSaveButton = new { Text = "SAVE" };
        [FindsBy(How = How.PartialLinkText, Using = "SAVE")]
        public IWebElement btn_editSaveButton;

        [FindsBy(How = How.Id, Using = "addToWishList")]
        public IWebElement con_addToWishListPopUp;

        [FindsBy(How = How.Id, Using = "DdlWishLists")]
        public IWebElement dd_selectWishListsDropDown;

        [FindsBy(How = How.ClassName, Using = "popupimg")]
        public IWebElement img_productImageatWishListPopUp;

        [FindsBy(How = How.ClassName, Using = "popupinfo")]
        public IWebElement con_ProductNameInfo;

        [FindsBy(How = How.Id, Using = "WishListContinueShopping")]
        public IWebElement btn_continueShoppingButton;

        [FindsBy(How = How.Id, Using = "ViewWishListDetails")]
        public IWebElement btn_viewWishListButton;

        [FindsBy(How = How.Id, Using = "newWishListName")]
        public IWebElement txt_newWishListNameField;

        [FindsBy(How = How.ClassName, Using = "forgotPassbuttons")]
        public IWebElement btn_wishListButtons;

        [FindsBy(How = How.Id, Using = "LblProductAdded")]
        public IWebElement lbl_labelProduct;

        [FindsBy(How = How.Id, Using = "createWhishListSuccess")]
        public IWebElement con_createWishListSuccessPopUp;

        [FindsBy(How = How.ClassName, Using = "overlay__container")]
        public IWebElement elm_addToWishListPopUpMouseOver;

        [FindsBy(How = How.ClassName, Using = "wish-list-overlay__image")]
        public IWebElement elm_productImageatWishListPopUpMouseOver;

        [FindsBy(How = How.ClassName, Using = "wish-list-overlay__info")]
        public IWebElement elm_ProductNameInfoMouseOver;

        [FindsBy(How = How.ClassName, Using = "create-wish-list-overlay__actions")]
        public IWebElement elm_wishListButtonsMouseOver;

        [FindsBy(How = How.Id, Using = "wish-list-name")]
        public IWebElement elm_newWishListNameFieldMouseOver;

        [FindsBy(How = How.ClassName, Using = "wish-list-overlay__message")]
        public IWebElement elm_wishListMessageMouseOver;

        [FindsBy(How = How.Id, Using = "stateRecyclingFee")]
        public IWebElement con_recyclingFeePopUp;

        [FindsBy(How = How.ClassName, Using = "manageWhishTable")]
        public IWebElement tbl_manageWishList;

        [FindsBy(How = How.Id, Using = "manageListsSaveBtn")]
        public IWebElement btn_saveButtonWishList;

        //public static object btn_createNewWishList = new { TagName = "span", Text = "CREATE NEW WISH LIST" };
        [FindsBy(How = How.LinkText, Using = "CREATE NEW WISH LIST")]
        public IWebElement btn_createNewWishList;

        //public static object btn_create = new { TagName = "a", Text = "CREATE" };
        [FindsBy(How = How.LinkText, Using = "CREATE")]
        public IWebElement btn_create;

        //public static object btn_cancel = new { TagName = "a", Text = "CANCEL" };
        [FindsBy(How = How.LinkText, Using = "CANCEL")]
        public IWebElement btn_cancel;

        //public static object btn_succesMessageClose = new { TagName = "a", Class = "close-reveal-modal" };
        // public static object lnk_manageWishList = new { TagName = "a", Class = "manageListLink" };
        [FindsBy(How = How.ClassName, Using = "manageListLink")]
        public IWebElement lnk_manageWishList;

        //public static object chk_deleteListTable = new { TagName = "table", Class = "manageWhishTable" };
        [FindsBy(How = How.ClassName, Using = "manageWhishTable")]
        public IWebElement chk_deleteListTable;

        [FindsBy(How = How.Id, Using = "manageListsSaveBtn")]
        public IWebElement btn_save;

        [FindsBy(How = How.Id, Using = "manageWishList")]
        public IWebElement btn_cancelParent;

        //public static object btn_cancelbtn = new { TagName = "a", Text = "CANCEL" };
        //public static object btn_closeOverlay = new { TagName = "a", Class = "close-reveal-modal" };
        [FindsBy(How = How.ClassName, Using = "close-reveal-modal")]
        public IWebElement btn_closeOverlay;

        //public static object lnk_listName = new { TagName = "a", Class = "manageList" };
        [FindsBy(How = How.ClassName, Using = "manageList")]
        public IList<IWebElement> lnk_listName;

        //public static object lnk_activeLists = new { TagName = "li", Class = "tab-title active" };
        [FindsBy(How = How.LinkText, Using = ("Wish Lists"))]
        public IWebElement lnk_activeLists;


        //a[.='Wish Lists']/ancestor::li[contains(@class,'tab-title')]
        [FindsBy(How = How.XPath, Using = ("//a[.='Wish Lists']/ancestor::li[contains(@class,'tab-title')]"))]
        public IWebElement lnk_activeTab;

       
        //public static object btn_AddToCart = new { TagName = "a", Text = "ADD TO CART" };
        [FindsBy(How = How.PartialLinkText, Using = "ADD TO CART")]
        public IWebElement btn_AddToCart;

        //public static object btn_ContinueShopping = new { TagName = "a", Text = "CONTINUE SHOPPING" };
        [FindsBy(How = How.PartialLinkText, Using = "CONTINUE SHOPPING")]
        public IWebElement btn_ContinueShopping;

        // public static object btn_RowWIshList = new { TagName = "span", Class = "whishListItemLinkBox" };
        [FindsByAll]
        [FindsBy(How = How.ClassName, Using = "whishListItemLinkBox")]
        public IList<IWebElement> btn_RowWishList;

        //public static object lnk_ActiveWishlist = new { Text= "Wish Lists" };
        [FindsBy(How = How.PartialLinkText, Using = "Wish Lists")]
        public IList<IWebElement> lnk_ActiveWishlist;

        [FindsBy(How = How.ClassName, Using = "global-promotions")]
        public IWebElement lbl_Global_Promotions;

        [FindsBy(How = How.XPath, Using = "/descendant::span[.= 'USE THIS ADDRESS'][last()]")]
        public IWebElement btn_UseThisAddress;


        //regression validations
        //public static object msg_LoadingInOrderHistory = new { Text = "Loading...",TagName="div"};
        [FindsBy(How = How.PartialLinkText, Using = "Loading...")]
        public IWebElement msg_LoadingInOrderHistory;

        // public static object btn_ViewOrderDetails = new { Text = "VIEW ORDER DETAILS", TagName = "a" };
        [FindsBy(How = How.PartialLinkText, Using = "VIEW ORDER DETAILS")]
        public IWebElement btn_ViewOrderDetails;

        [FindsBy(How = How.ClassName, Using = "msax-ProductDetails")]
        public IWebElement Img_OrderedProduct;

        [FindsBy(How = How.ClassName, Using = "confirmationOrder")]
        public IWebElement ele_ConfirmationNumber;

        [FindsBy(How = How.ClassName, Using = "msax-ItemsGrid")]
        public IWebElement grid_ProductDetails;

        [FindsByAll]
        [FindsBy(How = How.Id, Using = "chooseSuggestedAddress")]
        public IList<IWebElement> useThisPopup;
        
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string deleteWishLIst()
        {
            try
            {
                Archive.WaitForElement(By.ClassName("manageListLink"));
                lnk_manageWishList.Click();
                Archive.WaitForPageLoad();
                List<IWebElement> deleteList = chk_deleteListTable.FindElements(By.Name("manageWishlistChk")).ToList();
                int deleteListCount = deleteList.Count();
                if (deleteListCount > 0)
                {
                    for (int i = 0; i < deleteListCount; i++)
                    {
                        deleteList[i].Click();
                    }


                    btn_save.Click();
                    Archive.WaitForPageLoad();
                }
                else
                {
                    List<IWebElement> ListParent = btn_cancelParent.GetDescendants();
                    int option_Index = ListParent.IndexOf(ListParent.First(x => x.Text == "CANCEL"));
                    ListParent[option_Index].Click();
                    Archive.WaitForPageLoad();

                }

                result = "deleting WIshlist is succesful";
            }
            catch (Exception ex)
            {
                result = "Failure in deleting WIshlist " + ex.Message + ex.StackTrace;
            }


            return result;


        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ListName"></param>
        /// <param name="EditName"></param>
        /// <param name="ShareEmail"></param>
        /// <param name="shareMessage"></param>
        /// <returns></returns>
        public string listNames(string ListName, string EditName = null, string ShareEmail = null, string shareMessage = null)
        {
            try
            {
                //List<IWebElement> ele_lnk_listName = lnk_listName.GetControls(PropertyExpression.StartsWith);
                int option_Index = lnk_listName.IndexOf(lnk_listName.First(x => x.Text.Contains(ListName)));
                if (ListName == "Edit List Name")
                {
                    lnk_listName[option_Index].Click();
                    Archive.WaitForElement(By.Id("TxtEditWishlistName"));
                    txt_editWishlistName.Clear();
                    txt_editWishlistName.SendKeys(EditName);
                    btn_editSaveButton.Click();
                    Archive.WaitForPageLoad();
                    Thread.Sleep(5000);
                }
                else if (ListName == "Delete This List")
                {
                    lnk_listName[option_Index].Click();
                    Archive.WaitForPageLoad();
                    Thread.Sleep(5000);
                }
                else
                {
                    lnk_listName[option_Index].Click();
                    Archive.WaitForElement(By.Id("shareWishlistTo"));
                    txt_shareWishlistTo.SendKeys(ShareEmail);
                    txt_shareWishlistMessage.SendKeys(shareMessage);
                    btn_shareWishListButton.Click();
                    Archive.WaitForPageLoad();
                    Thread.Sleep(5000);

                }

                result = ListName + "is Performed succesfully";
            }
            catch (Exception ex)
            {
                result = "Failure in performing" + ListName + ex.Message + ex.StackTrace;
            }


            return result;

        }

        /// <summary>
        /// New wish lists can be created
        /// </summary>
        /// <param name="wishListName"></param>
        /// <returns></returns>
        public string createNewWishList(string wishListName)
        {
            try
            {
                WelcomePage WelcomePage = new WelcomePage();
                WelcomePage.btn_createNewWishList.Click();
                Archive.WaitForPageLoad();
                WelcomePage.txt_newWishListNameField.SendKeys(wishListName);
                WelcomePage.btn_create.Click();
                Archive.WaitForPageLoad();
                Archive.WaitForElement(By.Id("createWhishListSuccess"));
                List<IWebElement> PopUpList = WelcomePage.con_createWishListSuccessPopUp.GetDescendants();
                int option_Index = PopUpList.IndexOf(PopUpList.First(x => x.Text.Contains("×")));
                PopUpList[option_Index].Click();
                Archive.WaitForPageLoad();
                Thread.Sleep(3000);

                result = "Create New WIsh List is Succesful";
            }
            catch (Exception ex)
            {
                result = "Failure in Creating New WIshList" + ex.Message + ex.StackTrace;
            }

            return result;

        }


        /// <summary>
     /// Shipping address book details can be entered
     /// </summary>
     /// <param name="firstName"></param>
     /// <param name="lastName"></param>
     /// <param name="address"></param>
     /// <param name="city"></param>
     /// <param name="state"></param>
     /// <param name="zipCode"></param>
     /// <param name="phoneNumber"></param>
     /// <param name="email"></param>
     /// <returns></returns>
        public string addShippingAddressBook(string firstName, string lastName, string address, string city, string state, string zipCode, string phoneNumber, string email)
        {
            try
            {
                Archive.WaitForElement(By.PartialLinkText("Address Book"));
                lnk_addressBook.Click();
                btn_addAddress.Click();
                txt_firstName.SendKeys(firstName);
                txt_lastName.SendKeys(lastName);
                txt_address.SendKeys(address);
                txt_city.SendKeys(city);
                var selectElement = new SelectElement(dd_state);
                selectElement.SelectByText(state);
                txt_zipCode.SendKeys(zipCode);
                txt_phoneNumber.SendKeys(phoneNumber);
                txt_email.SendKeys(email);
                txt_confirmEmail.SendKeys(email);
                btn_editSaveButton.Click();
                Thread.Sleep(3000);
                if (btn_UseThisAddress.Displayed)
                {
                    btn_UseThisAddress.Click();
                }
                else
                {
                    Console.Out.WriteLine("Use This Address absence");
                }
                Archive.WaitForPageLoad();
                result = "Adding Shipping addressbook details are Performed Succesful";
            }
            catch (Exception ex)
            {
                result = "Failure in adding Shipping addressbook details" + ex.Message + ex.StackTrace;
            }
            return result;
        }


        /// <summary>
        /// Billing Address book details can be entered
        /// </summary>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <param name="address"></param>
        /// <param name="city"></param>
        /// <param name="state"></param>
        /// <param name="zipCode"></param>
        /// <param name="phoneNumber"></param>
        /// <param name="email"></param>
        /// <returns></returns>
        public string addBillingAddressBook(string firstName, string lastName, string address, string city, string state, string zipCode, string phoneNumber, string email)
        {
            try
            {
                Archive.WaitForElement(By.PartialLinkText("(Add Billing Address)"));
                IJavaScriptExecutor jse = (IJavaScriptExecutor)Config.driver;
                jse.ExecuteScript("window.scrollBy(0,-1000)", "");
                lnk_addBillingAddress.Click();
                txt_firstName.SendKeys(firstName);
                txt_lastName.SendKeys(lastName);
                txt_address.SendKeys(address);
                txt_city.SendKeys(city);
                SelectElement dropdown = new SelectElement(dd_state);
                dropdown.SelectByText(state);
                txt_zipCode.SendKeys(zipCode);
                txt_billingPhoneNumber.SendKeys(phoneNumber);
                txt_billingEmail.SendKeys(email);
                txt_billingConfirmEmail.SendKeys(email);
                btn_editSaveButton.Click();
                Thread.Sleep(6000);
                if (btn_UseThisAddress.Displayed)
                {
                    btn_UseThisAddress.Click();
                }
                else
                {
                    Console.Out.WriteLine("Use This Address absence");
                }
                Archive.WaitForPageLoad();
                Thread.Sleep(2000);
                result = "Adding Billing addressbook details are Performed Succesful";
            }
            catch (Exception ex)
            {
                result = "Failure in adding Billing addressbook details" + ex.Message + ex.StackTrace;
            }

            return result;
        }
    }
}





